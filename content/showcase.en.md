---
title: Showcase
disableToc: true
---

In 2020 the ESL initiative published the efforts and the current developments[^1].


[^1]: M.J.T Oliveira, N. Papior, Y. Pouillon, *et.al.*,
	*The CECAM electronic structure library and the modular software development paradigm*
    J. Chem. Phys. **153**, 024117 (2020),
    [DOI: 10.1063/5.0012901](https://doi.org/10.1063/5.0012901)
