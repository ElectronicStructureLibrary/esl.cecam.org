+++
title = "About ESL"
date = 2020-05-12T20:48:47+01:00
weight = 5
tags = ['Information']
+++

**ESL --- The Electronic Structure Library** is a community-maintained
library of software of use for electronic structure simulations. It is
an extended library that can be employed by everyone for building their
own packages and projects. It consists of entries documenting
*functionalities*, *algorithms*, *interfaces*, *standards* and *pieces
of code* ranging from small routines for performing simple tasks, all
the way up to complete libraries.

The ambition of the ESL is to segregate layers of functionality within
modules which are *general*, *standardised* and *efficient*. In this
way, new ideas, and new science, can be coded by scientists without
needing to rewrite functionalities that are already well-established,
and without needing to know more software engineering than science.

**Publication** --- The background, vision and content of the ESL are described in a 2020 paper:

Micael J. T. Oliveira, Nick Papior, Yann Pouillon, Volker Blum, Emilio Artacho, Damien Caliste, Fabiano Corsetti, Stefano de Gironcoli, Alin M. Elena, Alberto García, Victor M. García-Suárez, Luigi Genovese, William P. Huhn, Georg Huhs, Sebastian Kokott, Emine Küçükbenli, Ask H. Larsen, Alfio Lazzaro, Irina V. Lebedeva, Yingzhou Li, David López-Durán, Pablo López-Tarifa, Martin Lüders, Miguel A. L. Marques, Jan Minar, Stephan Mohr, Arash A. Mostofi, Alan O’Cais, Mike C. Payne, Thomas Ruh, Daniel G. A. Smith, José M. Soler, David A. Strubbe, Nicolas Tancogne-Dejean, Dominic Tildesley, Marc Torrent, and Victor Wen-zhe Yu,
	*The CECAM electronic structure library and the modular software development paradigm*
    J. Chem. Phys. **153**, 024117 (2020),
    [DOI: 10.1063/5.0012901](https://doi.org/10.1063/5.0012901)

If you use the ESL, please consider giving credit to this publication.

**Electronic Structure Codes that support the ESL** --- Codes that use the ESL or parts of it include
many major packages in our community. Examples include [abinit](https://www.abinit.org/),
[BigDFT](http://bigdft.org/Wiki/index.php?title=BigDFT_website), [FHI-aims](https://aimsclub.fhi-berlin.mpg.de/),
[GPAW](https://wiki.fysik.dtu.dk/gpaw/), [Octopus](https://octopus-code.org/wiki/Main_Page),
[Quantum Espresso](https://www.quantum-espresso.org/),
[Siesta](https://departments.icmab.es/leem/siesta/), and others. The ESL is by no means restricted to this set - the more, the better.

Contents of the ESL
-------------------

The ESL is not intended to be a conventional software library. Our aim
is for it to grow with contributions open to a wide community, without
imposing the rigidity implied by the *a priori* definition of a
conventional library. It is also intended to be more than a software
repository. The expectation is for the ESL to be useful and used, and
through that to be able to slowly standardise software, APIs and data
standards of common use in the community, as opposed to accumulating
disconnected bits of code.

The contents of the library is organised in the form of ESL entries,
with one page per entry. ESL entries are categorised as
`Functionalities`, `Data standards`, `ESL Software`, and
`Other Software` (listed as category tags). One entry can contain more
than one of these category. However,
we encourage the creation of single-category entries linked to one
another, in order to allow a navigable network to form. In particular,
entries describing functionalities should be separated as much as
possible, since each functionality will typically link to several
different implementations.

### Functionalities

Describes a particular functionality typically taken care of by a piece
of software. This could be anything from a small routine up to a
complete library. It should represent a specific process or component of
an electronic structure calculation, but may be broad enough to allow
different approaches/methods/algorithms or a variety of smaller tasks
within it. Examples are:

-   Molecular dynamics, many possible
    algorithms for different ensembles, thermostats, etc.
-   Linear algebra, a collection of
    mathematical operations within a single framework
-   Memory management, a general
    software requirement allowing for various approaches and tasks

### Data standards

Contains the specifications for storing particular types of data in
files. It should provide all the necessary details for any program to be
able to read and write according to the standard.

### ESL Software/Other Software

Describes and links to a piece of code. The code can be specific to a
single functionality or bundle together more than one of each. The
important things to keep in mind for these category are:

-   A download link must be included in the page.
-   Details of the license and code authors must be listed.
-   Appropriate documentation should be given either in the page
    itself, or by linking to an external page.

ESL Software is either software born and developed within the ESL
initiative or software distributed under the ESL brand with the
agreement of its authors. Other Software is software that is not part of
the ESL, but is nevertheless of interest for anyone developing or using
electronic structure codes.

Contributing to the ESL
-----------------------

Creating and maintaining the ESL entries is open to all. If you would
like to do so, please read through the
[Contributing]({{< ref "/contrib/_index.md" >}}) page for more details. Have a
look around the existing entries to get a feel for how the division in
categories works in practice.

You are free to create and contribute to categories at all levels. You
can also organise your contribution as you prefer. Here are some
suggestions:

-   If you have a piece of code you would like to add: it is common to
    create one entry describing the algorithm, API and software
    documentation for your code, and linking to a second, smaller entry
    describing the overarching functionality. If an appropriate
    functionality entry already exists, simply add a link to your code
    entry, and tweak its contents if necessary.
-   If you have a functionality or a data standard you would like to
    propose, but no code: create a entry describing it.
-   If you have a large library with many different components: consider
    dividing your contribution between many small entries at different
    levels. The more you can make the separation in the pages between
    different functionalities the easier it will be for other people to
    link their projects to existing pages and help the network grow as a
    coherent whole.
