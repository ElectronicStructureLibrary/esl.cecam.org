---
title: Contact
disableToc: true
---

The ESL comprises a set of entities governing the day-2-day workflow.

To contact ESL please write us [here](mailto:esl@cecam.org).

Additionally we have a Slack channel where you can interact with us: [ESL slack](https://esl-cecam.slack.com).
(The slack channel will be discontinued shortly and moved to Zulip)


## Curating team

The curating team takes care of day to day work and has monthly meetings. Typically the first
Thursday of the month @ 14 or 15 CEST.

If you want to participate in our monthly meetings, please let us know.

### Members

+ [Micael Oliveira](https://www.mpsd.mpg.de/person/48375/2736), Max Planck Institute for the Structure and Dynamics of Matter, Hamburg, Germany
+ [Yann Pouillon](https://www.simuneatomistics.com/about-us/), Simune Atomistics, Spain
+ [Nick Rubner Papior](https://orbit.dtu.dk/en/persons/nick-r%C3%BCbner-papior/), Technical University of Denmark
+ [Damien Caliste](https://www.mem-lab.fr/en/Pages/Portrait/Damien-Caliste.aspx), CEA, Grenoble, France
+ [Volker Blum](https://mems.duke.edu/faculty/volker-blum), Duke University, USA
+ [Alin Marin Elena](https://www.scd.stfc.ac.uk/Pages/Alin-Elena.aspx), Daresbury Laboratory, UK
+ [Martin Lüders](https://www.mpsd.mpg.de/person/98716/2736), Max Planck Institute for the Structure and Dynamics of Matter, Hamburg, Germany
+ [Pietro Delugas](), SISSA, Italy

## Steering committee

The ESL is governed by the steering committee which is comprised of library authors of all ESL libraries.

This entity has 3-4 yearly meetings.


### Members

+ [Alberto Garcia](https://departments.icmab.es/leem/alberto/), Institut de Ciencia de Materials de Barcelona, Spain
+ [Luigi Genovese](), CEA Grenoble, France
+ [Xavier Gonze](https://perso.uclouvain.be/xavier.gonze/), UC Louvain, Belgium
+ [Stefano de Gironcoli](https://people.sissa.it/~degironc/), SISSA, Trieste, Italy
+ [Micael Oliveira](https://www.mpsd.mpg.de/person/48375/2736), Max Planck Institute for the Structure and Dynamics of Matter, Hamburg, Germany
+ [Nick Rubner Papior](https://orbit.dtu.dk/en/persons/nick-r%C3%BCbner-papior/), Technical University of Denmark
+ [Damien Caliste](https://www.mem-lab.fr/en/Pages/Portrait/Damien-Caliste.aspx), CEA, Grenoble, France
+ [Yann Pouillon](https://www.simuneatomistics.com/about-us/), Simune Atomistics, Spain
+ [Volker Blum](https://mems.duke.edu/faculty/volker-blum), Duke University, USA
+ [Ask Hjorth Larsen](), Technical University of Denmark
+ [Victor Yu](https://scholars.duke.edu/person/wenzhe.yu), Argonne National Laboratory, USA
+ [Layla Martin-Samos](), SISSA, Italy
