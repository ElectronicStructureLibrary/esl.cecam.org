---
title: Credits
disableToc: true
---

## Contributors

The ESL and the libraries contributed to the ESL have been developed by a large amount of people. The names are listed in a non-particular order.

Emilio Artacho, Volker Blum, Damien Caliste, Fabiano Corsetti, Stefano de Gironcoli, Alin M. Elena, Alberto García, Victor M. García-Suárez, Luigi Genovese, William Huhn, Georg Huhs, Sebastian Kokott, Emine Küçükbenli, Ask H. Larsen, Alfio Lazzaro, Irina V. Lebedeva, Yingzhou Li, David López-Durán, Pablo López-Tarifa, Martin Lüders, Miguel Marques, Jan Minar, Stephan Mohr, Arash Mostofi, Alan O’Cais, Micael Oliveira, Nick Papior, Mike Payne, Yann Pouillon, Thomas Ruh, Daniel Smith, José Soler, David Strubbe, Nicolas Tancogne-Dejean, Dominic Tildesley, Marc Torrent, and Victor Wen-zhe Yu.

Additionally the ESL initiative is supported and helped by CECAM. The CECAM
infrastructure support is thanked and comprises (at least): - Sara Bonella,
Bogdan Nichita, and Ignacio Pagonabarraga

Additional contributions are acknowledged:

Luis Agapito, Xavier Andrade, Balint Aradi, Emanuele Bosoni, Lori A. Burns, Christian Carbogno, Ivan Carnimeo, Abel Carreras Conill, Alberto  Castro, Michele Ceriotti, Anoop Chandran, Wibede Jong, Pietro Delugas, Thierry Deutsch, Hubert Ebert, Aleksandr Fonari, Luca Ghiringhelli, Paolo Giannozzi, Matteo Giantomassi, Judit Gimenez, Ivan  Girotto, Xavier Gonze, Benjamin Hourahine, Jürg Hutter, Thomas Keal, Jan Kloppenburg, Hyungjun Lee, Liang Liang, Lin Lin, Jianfeng Lu, Nicola Marzari, Donal MacKernan, Layla Martin-Samos, Paolo Medeiros, Fawzi Mohamed, Jens Jørgen Mortensen, Sebastian Ohlmann, David O’Regan, Charles Patterson, Etienne Plésiat, Markus Rampp, Laura Ratcliff, Stefano Sanvito, Paul Saxe, Matthias Scheffler, Didier Sebilleau, Søren Smidstrup, James Spencer, Atsushi Togo, Joost Vandevondele, Matthieu Verstraete, Brian Wylie.
