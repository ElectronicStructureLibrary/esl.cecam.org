+++
title = "Events"
date = 2020-05-12T21:20:22+01:00
weight = 10
chapter = false
+++

## Workshops

Much of the work on ESL happens in workshops that are organized at least yearly. The format is not the traditional format of a workshop with
only talks and presentations. Rather, large segments of these workshops are dedicated to actual coding and development.

The key point is that these workshops are open to anyone with an interest in library development for electronic structure theory. If
this is something you are doing, we would like to see you there. Together, we can make a great shared resource to simplify the
nitty-gritties of electronic structure theory and push the science forward.

### Upcoming

* [Electronic Structure Software Development: Advancing the Modular Paradigm](https://www.cecam.org/workshop-details/1194)(ESL10) February 19, 2024  - March 1, 2024, [CECAM](https://www.cecam.org), EPFL, Lausanne, Switzerland.

### Previous

* [Extended Software Development Workshop: Best practices and tools](https://www.cecam.org/workshop-details/1121) (ESL9) October 10, 2022 - October 21, 2022, [CECAM](https://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Extended Software Development Workshop: Improving bundle libraries](https://www.cecam.org/workshop-details/23) (ESL8) October 11, 2021 - October 22, 2021, [CECAM](https://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Extended Software Development Workshop: Integration of ESL modules into electronic-structure codes](https://www.cecam.org/workshop-details/85) (ESL7) February 17, 2020 - February 28, 2020, [CECAM](https://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Extended Software Development Workshop: Scaling Electronic Structure Applications](https://www.cecam.org/workshop-details/180) (ESL6) January 7 to January 18, 2019 [CECAM-IRL](https://www.cecam.org/node_cecamirl.html), University College Dublin, Dublin, Ireland.

* [Electronic Structure Library coding workshop: ESL demonstrator](https://www.cecam.org/workshop-details/293) (ESL5) February 5 to February 16, 2018 [CECAM](http://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Electronic Structure Library coding workshop: drivers](https://www.cecam.org/workshop-details/287) (ESL4) July 10 to July 21, 2017 [SISSA](http://www.sissa.it), Trieste, Italy.

* [Electronic Structure Library coding workshop: solvers](https://www.cecam.org/workshop-details/390) (ESL3) June 06 to June 17, 2016 [ZCAM](http://www.z-cam.es), University of Zaragoza, Spain.

* [Towards a common format for computational materials science data](https://www.cecam.org/workshop-details/378) January 25 to February 05, 2016 [CECAM](http://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Electronic Structure Library coding workshop: utilities toolbox](https://www.cecam.org/workshop-details/452) (ESL2) June 01 to June 05, 2015 [CECAM](http://www.cecam.org), EPFL, Lausanne, Switzerland.

* [Extended software development workshop](https://www.cecam.org/workshop-details/515) (ESL1) June 30 to July 02, 2014 [CECAM](http://www.cecam.org), EPFL, Lausanne, Switzerland.


## Fortran OOP Seminar Series

Several ESL developers meet regularly to talk about Object-Oriented Programming with Fortran. These are rather informal meetings covering a wide range of topics, where participants share their experiences, present problems and ask for advice.

* Seminar 1, November 16, 2021. Presentation by Bálint Aradi, ["Geometry Drive Pattern in DFTB\+"](ESL-FortranOOP-Balint_Aradi.pdf).
* Seminar 2, December 17, 2021. Presentation by Nicolas Salles, ["LAMMPS architecture in FORTRAN"](LAMMPS_in_FORTRAN-Nicolas_Salles.pdf)
* Seminar 3, January 14, 2022. Presentation by Micael Oliveira, ["Octopus Multi-system Framework"](Octopus_Multisystem-Micael_Oliveira.pdf)
* Seminar 4, February 17, 2022. Presentation by Sebastian Ehlert, ["Using objects across language boundaries"](https://awvwgk.github.io/foopss/)
* Seminar 5, May 12, 2022. Presentation by Ben Hourahine, ["MPI/OpenMP patterns in DFTB+"](MPI-OpenMP_patterns_in_DFTB+-Ben_Hourahine.pdf)
* Seminar 6, June 23. Round-table about code refactoring.

Topics for upcoming seminars:
* Object creation, instantiation techniques (discussing Fortran related issues + general patterns like abstract factory, etc.)
* Interface designing HOWTO.
* Refactoring HOWTO.
* Testing strategies
* Good practices in operator overloading
