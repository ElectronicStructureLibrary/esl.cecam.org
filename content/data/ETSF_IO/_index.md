+++
title = "ETSF_IO"
date =  2020-05-12T21:36:07+01:00
weight = 30
tags = ["Data standards","ESL Entries", "I/O"]
+++

One of the objectives of the [European Theoretical Spectroscopy
Facility](http://www.etsf.eu) is to specify file formats for the
contents that are relevant to the scientific activity of its
constituting nodes. The present document describes detailed NetCDF
specifications, for selected contents
(crystallographic/density/wavefunctions). It is hoped that these
specifications will be implemented in many different softwares, or (at
least) will be the basis of even better file format specifications.

Introduction
------------

This document has the goal of informing the electronic structure
community of the agreed ETSF specifications, hereafter referred to as
SpecFF\_ETSF3, in view of further discussions and implementations. It is
expected that the file format specifications present in this document be
subject to revision and improvements. The first version of this
document, named SpecFFNQ1 (NQ for Nanoquanta, the precursor of the
ETSF), was frozen around June 2006. The current version of the file
format, and associated information, can be found at
<http://www.etsf.eu/fileformats>.

The document is organized in sections:

-   Section [General considerations]({{< relref "_index.md#General_considerations" >}})
    presents general considerations concerning the present file format
    specifications.
-   Section [General specifications]({{< relref "_index.md#General_specifications" >}})
    presents general specifications concerning ETSF NetCDF file formats.
-   Section [Specification for files containing crystallographic
    data]({{< relref "_index.md#Specification_for_files_containing_crystallographic_data" >}})
    deals with files containing crystallographic data, and present a
    rather detailed NetCDF specification.
-   Section [Specification for files containing a density or a
    potential]({{< relref "_index.md#Specification_for_files_containing_a_density_or_a_potential" >}})
    deals with files containing density/potential, with the same level
    of detail.
-   Section [Specification for files containing the
    wavefunctions]({{< relref "_index.md#Specification_for_files_containing_the_wavefunctions" >}})
    deals with files containing wavefunctions, with the same level of
    detail.
-   Section [Dielectric function]({{< relref "_index.md#Dielectric_function" >}}) deals
    with files containing dielectric functions and related data.

General considerations
----------------------

One has to consider separately the *set of data* to be included in each
of different types of files, from their *representation*. Concerning the
latter, one encounters simple text files, binary files, XML-structured
files, NetCDF files, etc \... The ETSF decided to evolve towards formats
that deal appropriately with the self- description issue, i.e. XML and
NetCDF. The inherent flexibility of these representations also allow to
evolve specific versions of each type of files progressively, and refine
earlier working proposals. The same direction has been adopted by
several groups of code developers that we know of.

Information on NetCDF and XML can be obtained from the official Web
sites:\
<http://www.unidata.ucar.edu/software/netcdf/> and <http://www.w3.org/XML/>

There are numerous other presentations of these formats on the Web, or
in books.

*Concerning XML*\
(A) The XML format is most adapted for the structured representation of
relatively small quantity of data, as it is not compressed.\
(B) It is a very flexible format.

*Concerning NetCDF*\
(A) Several groups of developers inside the ETSF have already a good
experience of using it, for the representation of binary data (large
files).\
(B) Although there is no clear advantage of NetCDF compared to HDF
(another possibility for large binary files), this experience inside the
ETSF network is the main reason for preferring it.\
(C) File size limitations of NetCDF exist, see [Appendix
A]({{< relref "_index.md#Appendix_A:_Some_information_on_the_NetCDF_size_limitation" >}}),
but should be limited to old architectures.

Thanks to the flexibility of NetCDF, the content of a NetCDF file format
suitable for use for ETSF softwares might be of four different types:\
(1) The actual numerical data (that defines a file for wavefunctions, or
a density file, etc \...), whose NetCDF description would have been
agreed.\
(2) The auxiliary data that are mandatory to make proper usage of the
actual numerical data. The NetCDF description of these auxiliary data
should also be agreed.\
(3) The auxiliary data that are not mandatory, but whose NetCDF
description has been agreed, in a larger context.\
(4) Other data, typically code-dependent, whose existence might help the
use of the file for a specific code. The name of these variables should
be different from the names chosen for agreed variables (1)-(3). Such
other data might even be redundant with (1)-(3).

Such content is compatible with a file format being complete for use by
many codes, though adapted for the specific usage by one code. The ETSF
file descriptions to be provided later (sections [General
specifications]({{< relref "_index.md#General_specifications" >}}) to [Dielectric
function]({{< relref "_index.md#Dielectric_function" >}})) are based on this generic
classification of data that can be integrated in such a NetCDF file.

In order to address the 2 GB limit (see [Appendix
A]({{< relref "_index.md#Appendix_A:_Some_information_on_the_NetCDF_size_limitation" >}})),
as well as the use of NetCDF files for parallel calculations, one file
can actually be split into several partial files. Selected variables
should describe the differing content of each of them. As an example, in
[Specification for files containing a density or a
potential]({{< relref "_index.md#Specification_for_files_containing_a_density_or_a_potential" >}}),
a file containing a set of wavefunctions can be split in different files
containing selected bands and/or k-points, however being exactly similar
in every other respect.

Some technical details concerning the use of NetCDF files apply to all
formats specified in the ETSF framework:

1.  concerning the variable names, long names should be chosen, as close
    as possible to natural language (so inherently self-descriptive).
2.  all variable names are lower case, except \"Conventions\" - a name
    agreed by the NetCDF community
3.  underscores are used to replace blanks separating words
4.  in the tables, the slow indices are left-most, and the fast indices
    are right-most, so that the *order of indices has to be reversed* in
    Fortran

General specifications
----------------------

### Global attributes

Global attributes are used for a general description of the file, mainly
the file format convention. Important data is not contained in
attributes, but rather in variables.

The length of character attributes is the maximum length this attribute
may take. This is relevant for reading, where sufficient space must be
provided. In writing, the defined length may be reduced to the real
length of the attribute.

#### Mandatory attributes

This table gathers specifications for required attributes in any ETSF
NetCDF files.

  Attributes             |Type (length)  |Notes
  -----------------------|---------------|--------------------------------------
  file\_format           |char (80)      |\"ETSF\"
  file\_format\_version  |real           |1.1 or 1.2 or 2.0 \...
  Conventions            |char (80)      |\"<http://www.etsf.eu/fileformats>\"

-   **file\_format**: Name of the file format for ETSF wavefunctions.
-   **file\_format\_version**: Real version number for file format (only
    one period, e.g. 1.2 ).
-   **Conventions**: NetCDF recommended attribute specifying where the
    conventions for the file can be found on the Internet.

#### Optional attributes

This table presents optional attributes for ETSF NetCDF files.

  Attributes  |Type (length)  |Notes
  ------------|---------------|-------
  history     |char (1024)    |
  title       |char (80)      |

-   **history**: NetCDF recommended attribute: each code
    modifying/writing this file is encouraged to add a line about itself
    in the history attribute. char(1024) allows for 12 additions of at
    most 80 characters.
-   **title**: Short description of the content (system) of the file.

Generic attributes of variables
-------------------------------

A few attributes might apply to a large number of variables. The
following table presents the generic attributes that might be mandatory
for selected variables in ETSF NetCDF files.

  Attributes                |Type (length)  |Notes
  --------------------------|---------------|------------------------------------------------
  units                     |char (80)      |required for variables that carry units
  scale\_to\_atomic\_units  |double         |required for units other than \"atomic units\"

-   **units**: It is one of the NetCDF recommended attributes, but it
    only applies to a few variables in our case, since most are
    dimensionless. For dimensional variables, it is required. The use of
    atomic units (corresponding to the string \"atomic units") is
    advised throughout for portability. If other units are used, the
    definition of an appropriate scaling factor to atomic units is
    mandatory. Actually, the definition of the name \"units" in the ETSF
    files is only informative: the \"scale\_to\_atomic\_units\"
    information should be the only one used to read the file by
    machines.
-   **scale\_to\_atomic\_units**: If \"units" is something other than
    the character string \"atomic units" (based on Hartree for energies,
    Bohr for lengths) we request the definition of an appropriate
    scaling factor. The appropriate value in atomic units is obtained by
    multiplying the number found in the variable by the scaling factor.
    Examples:\
    units=\"eV" $\rightarrow$ scale\_to\_atomic\_units = 0.036749326\
    units=\"angstrom" $\rightarrow$ scale\_to\_atomic\_units =
    1.8897261\
    units=\"parsec\" $\rightarrow$ scale\_to\_atomic\_units =
    5.8310856e+26\
    This can be used to deal with unknown units. Note that the
    recommended values for the fundamental constants can be found at
    [NIST](http://physics.nist.gov/cuu/Constants/index.html).

### Flag-like attributes

\"Flag-like" attributes can take the values \"yes" and \"no". When such
attributes are written, they should be written in full length and small
letters. When they are read, only the first character needs to be
checked (i.e. \"y" or \"n\" -- this simplifies life a lot).

### Dimensions

Dimensions are used for one- or multidimensional variables. It is very
important to remember that the NetCDF interface adapts the dimension
ordering to the programming language used. The notation here is C-like,
i.e. the last index varies fastest. In Fortran, the order is reversed.
When implementing new reading interfaces, the dimension names can be
used to check the dimension ordering. The dimension names help also to
identify the meaning of certain dimensions in cases where the number
alone is not sufficient.

The variables that specify dimensions in ETSF files are divided into two
lists: one for the dimensions that are not supposed to lead to a
splitting, and another for the dimensions that might be used to define a
splitting (e.g. in case of parallelism).

#### Dimensions that cannot be split

This table list the dimensions that are not supposed to lead to a
splitting.

  Dimensions                          |Type (index order as in C)  |Notes
  ------------------------------------|----------------------------|-----------------
  character\_string\_length           |integer                     |Always ==80
  real\_or\_complex\_coefficients     |integer                     |Either ==1 or 2
  real\_or\_complex\_density          |integer                     |Either ==1 or 2
  real\_or\_complex\_gw\_corrections  |integer                     |Either ==1 or 2
  real\_or\_complex\_potential        |integer                     |Either ==1 or 2
  real\_or\_complex\_wavefunctions    |integer                     |Either ==1 or 2
  number\_of\_cartesian\_directions   |integer                     |Always ==3
  number\_of\_reduced\_dimensions     |integer                     |Always ==3
  number\_of\_vectors                 |integer                     |Always ==3
  number\_of\_symmetry\_operations    |integer                     |
  number\_of\_atoms                   |integer                     |
  number\_of\_atom\_species           |integer                     |
  symbol\_length                      |integer                     |Always ==2

-   **character\_string\_length**: The maximum length of string
    variables (attributes may be longer).
-   **real\_or\_complex\_coefficients**: To specify whether the variable
    coefficients\_of\_wavefunctions
    ([Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})) is real or complex.
-   **real\_or\_complex\_density**: To specify whether the variable
    density ([Density]({{< relref "_index.md#Density" >}})) is real or complex.
-   **real\_or\_complex\_gw\_corrections**: To specify whether the
    variable gw\_corrections ([BSE/GW]({{< relref "_index.md#BSE/GW" >}})) is real or
    complex.
-   **real\_or\_complex\_potential**: To specify whether the variables
    exchange\_potential, correlation\_potential, and
    exchange\_correlation\_potential ([Exchange and
    correlation]({{< relref "_index.md#Exchange_and_correlation" >}})) are real or
    complex.
-   **real\_or\_complex\_wavefunctions**: To specify whether the
    variable real\_space\_wavefunctions
    ([Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})) is real or complex.
-   **number\_of\_cartesian\_directions**: Used for absolute
    coordinates.
-   **number\_of\_reduced\_dimensions**: Used for reduced (also called
    relative) coordinates in reciprocal or real space.
-   **number\_of\_vectors**: Used to distinguish the vectors when
    defining their relative / reduced coordinates.
-   **number\_of\_symmetry\_operations**: The number of symmetry
    operations.
-   **number\_of\_atoms**: The number of atoms in the unit cell.
-   **number\_of\_atom\_species**: The number of different atom species
    in the unit cell.
-   **symbol\_length**: Maximum number of characters for the chemical
    symbols.

#### Dimensions that can be split

This table list the dimensions that might be used to define a splitting
(e.g. in case of parallelism). For the auxiliary variables needed in
case of splitting, see [Splitting]({{< relref "_index.md#Splitting" >}}).

  Dimensions                       |Type (index order as in C)  |Notes
  ---------------------------------|----------------------------|----------------------------------------------------------------------------------------------------------------------------------------
  max number of states             |integer                     |
  number of kpoints                |integer                     |
  number of spins                  |integer                     |Either ==1 or 2
  number of spinor components      |integer                     |Either ==1 or 2
  number of components             |integer                     |Either ==1, 2 or 4
  max number of coefficients       |integer                     |
  number of grid points vector1    |integer                     |
  number of grid points vector2    |integer                     |
  number of grid points vector3    |integer                     |
  max number of basis grid points  |integer                     |For wavelets. Range in 1 to number\_of\_grid\_points1\_vector1 * number\_of\_grid\_points1\_vector2 * number\_of\_grid\_points1\_vector3
  number of localisation regions   |integer                     |Always 1.

-   **max\_number\_of\_states**: The maximum number of states.
-   **number\_of\_kpoints**: The number of kpoints.
-   **number\_of\_spins**: Used to distinguish collinear spin-up and
    spin-down components:\
    1 for non-spin-polarized or spinor wavefunctions\
    2 for collinear spin (spin-up and spin-down).
-   **number\_of\_spinor\_components**: For non-spinor wavefunctions,
    this dimension must be present and equal to 1. For spinor
    wavefunctions this dimension must equal to 2.
-   **number\_of\_components**: Used for the spin components of
    spin-density matrices:\
    1 for non-spin-polarized\
    2 for collinear spin (spin-up and spin-down)\
    4 for non-collinear spin (average density, then magnetization vector
    in cartesian coordinates x,y and z).
-   **max\_number\_of\_coefficients**: The (maximum) number of
    coefficients for the basis functions at each k-point, except in the
    case of real space grids (see next lines).
-   **number\_of\_grid\_points\_vector1**: The number of grid points
    along direction 1 in the unit cell in real space, for dimensioning
    the wavefunction coefficients (an alternative to
    max\_number\_of\_coefficients).
-   **number\_of\_grid\_points\_vector2**: Same as
    number\_of\_grid\_points\_vector1, for the second direction.
-   **number\_of\_grid\_points\_vector3**: Same as
    number\_of\_grid\_points\_vector1, for the third direction.
-   **max\_number\_of\_basis\_grid\_points**: For wavelets. The number
    of relevant points from the regular mesh in real space used to store
    coefficients. This value is the maximum of number of basis set grid
    points over all localization region(s). Currently,
    number\_of\_localization\_regions is always 1, so
    max\_number\_of\_basis\_grid\_points is simply the number of basis
    set grid points.
-   **number\_of\_localization\_regions** For wavelets. This dimension
    will be used later to define one or several localized basis sets.

To clarify the interplay between number\_of\_spins,
number\_of\_components, and number\_of\_spinor\_components, note the
different following magnetic or non-magnetic cases:\
Non-spin-polarized:\
number\_of\_spins=1 , number\_of\_spinor\_components=1,
number\_of\_components=1\
Collinear spin-polarized:\
number\_of\_spins=2, number\_of\_spinor\_components=1,
number\_of\_components=2\
Non-collinear spin-polarized:\
number\_of\_spins=1, number\_of\_spinor\_components=2,
number\_of\_components=4

#### Splitting

We now turn to the specification of the (optional) splitting of files in
partial files. Such splitting might be done in many different ways. In
order to allow for very general, flexible, splittings, but still rely on
a simple system, we set up different pairs of variables, one for each
possible splitting. These pairs of variables are described in [Auxiliary
dimensions for
splitting]({{< relref "_index.md#Auxiliary_dimensions_for_splitting" >}}) and
[Auxiliary variables for
splitting]({{< relref "_index.md#Auxiliary_variables_for_splitting" >}}). If a software
cannot cope with the file splitting, it should simply check that no file
splitting is done, and if the contrary happens, it should stop.

Let us work out an example.\
Suppose we split the file according to the kpoints. The full set might
have 10 kpoints, of which 3 kpoints (number 1, 2 and 5) might be
contained in a first file, 3 other kpoints (number 3, 6 and 9) might be
contained in a second file, and the 4 remaining kpoints (number 4, 7, 8
and 10) might be contained in the third file.

Then, the first file will contain:\
number\_of\_kpoints = 10 , my\_number\_of\_kpoints = 3 ,
my\_kpoints=(1,2,5)

The second file will contain:\
number\_of\_kpoints = 10 , my\_number\_of\_kpoints = 3 ,
my\_kpoints=(3,6,9)

The third file will contain:\
number\_of\_kpoints = 10 , my\_number\_of\_kpoints = 4 ,
my\_kpoints=(4,7,8,10)

If more than one splitting is done, the file will contain the
intersection of the split data. As an example, suppose we split the file
according to the kpoints and the spins. The full set of kpoints might
have 4 kpoints, and there would be two spins. We perform two splittings,
one separating kpoints 1 and 2 from kpoints 3 and 4, and one separating
the spins.\
The first file might contain:\
number\_of\_kpoints = 4 , my\_number\_of\_kpoints = 2 ,
my\_kpoints=(1,2)\
number\_of\_spins = 2 , my\_number\_of\_spins = 1 , my\_spins=(1)

The second file might contain:\
number\_of\_kpoints = 4 , my\_number\_of\_kpoints = 2 ,
my\_kpoints=(3,4)\
number\_of\_spins = 2 , my\_number\_of\_spins = 1 , my\_spins=(1)

The third file might contain:\
number\_of\_kpoints = 4 , my\_number\_of\_kpoins = 2 ,
my\_kpoints=(1,2)\
number\_of\_spins = 2 , my\_number\_of\_spins = 1 , my\_spins=(2)

The fourth file might contain:\
number\_of\_kpoints = 4 , my\_number\_of\_kpoins = 2 ,
my\_kpoints=(3,4)\
number\_of\_spins = 2 , my\_number\_of\_spins = 1 , my\_spins=(2)

Different variables might change their sizes when splitting is used. The
list of variables whose size might change compared to non-split files
will have to be specified.

##### Auxiliary dimensions for splitting

Dimensions of variables to specify the (optional) splitting of one file
in different partial files. These dimensions and associated variables
(see [Auxiliary variables for
splitting]({{< relref "_index.md#Auxiliary_variables_for_splitting" >}})) are defined
by pair (one integer, and one integer array). Any one of these pairs can
be used to split the files, and several of these pairs can be used as
well. In case several pairs are used, the content of the file is defined
by the intersection of the different integer arrays. The detailed
description of these variables is induced from the one of the
corresponding variables in [Dimensions that can be
split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}).

  Dimensions                             |Type (index order as in C)  |Notes
  ---------------------------------------|----------------------------|---------------------------------------------------
  my\_max number\_of\_states             |integer                     |At least 1, at most number of states
  my\_number\_of\_kpoints                |integer                     |At least 1, at most number of kpoints
  my\_number\_of\_spins                  |integer                     |At least 1, at most number of spins
  my\_number\_of\_spinor\_components     |integer                     |At least 1, at most number of spinor components
  my\_number\_of\_components             |integer                     |At least 1, at most number of components
  my\_number\_of\_grid\_points\_vector1  |integer                     |At least 1, at most number of grid points vector1
  my\_number\_of\_grid\_points\_vector2  |integer                     |At least 1, at most number of grid points vector2
  my\_number\_of\_grid\_points\_vector3  |integer                     |At least 1, at most number of grid points vector3
  my\_max\_number\_of\_coefficients      |integer                     |At least 1, at most max number of coefficients

##### Auxiliary variables for splitting

Variables to specify the (optional) splitting of one file in different
partial files. See the explanation in [Auxiliary dimensions for
splitting]({{< relref "_index.md#Auxiliary_dimensions_for_splitting" >}}). The detailed
description of these variables is induced from the one of the
corresponding variables from [Dimensions that can be
split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}).

  Variables                  |Type (index order as in C)                         |Notes
  ---------------------------|---------------------------------------------------|-------
  my\_states                 |integer \[my\_max\_number\_of\_states\]            |
  my\_kpoints                |integer \[my\_number\_of\_kpoints\]                |
  my\_spins                  |integer \[my\_number\_of\_spins\]                  |
  my\_spinor\_components     |integer \[my\_number\_of\_spinor\_components\]     |
  my\_components             |integer \[my\_number\_of\_components\]             |
  my\_grid\_points\_vector1  |integer \[my\_number\_of\_grid\_points\_vector1\]  |
  my\_grid\_points\_vector2  |integer \[my\_number\_of\_grid\_points\_vector2\]  |
  my\_grid\_points\_vector3  |integer \[my\_number\_of\_grid\_points\_vector3\]  |
  my\_coefficients           |integer \[my\_max\_number\_of\_coefficients\]      |

### Optional variables

In order to avoid the "divergence of the formats in the additional
data", we propose names and formats for some information that is likely
to be written to the files. This section will grow in future format
versions. Please report any variable you miss here, so we can add it to
the list. None of these data is mandatory for the file formats to be
described later. Some of the proposed variables contain redundant
information.

All optional variables must be defined BEFORE the largest size array of
the file, otherwise this array will be restricted to 4GB. Examples of
such arrays are **coefficients\_of\_wavefunctions** or
**real\_space\_wavefunctions** (see later).

These optional variables are grouped with respect to their physical
relevance: atomic information, electronic structure, and reciprocal
space.

#### Atomic information

  Variables               |Type (index order as in C)                                       |Notes
  ------------------------|-----------------------------------------------------------------|-------
  valence\_charges        |double \[number\_of\_atom\_species\]                             |
  pseudopotential\_types  |char \[number\_of\_atom\_species\]\[character\_string\_length\]  |

-   **valence\_charges**: Ionic charges for each atom species.
-   **pseudopotential\_types**: Type of pseudopotential scheme =
    \"bachelet-hamann-schlueter", \"troullier-martins", \"hamann",
    \"hartwigsen-goedecker-hutter", \"goedecker-teter-hutter\"\... A
    standardized list should be found or established.

#### Electronic structure

  Variables                |Type (index order as in C)          |Notes
  -------------------------|------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  number of electrons      |integer                             |
  exchange\_functional     |char \[character\_string\_length\]  |
  correlation\_functional  |char \[character\_string\_length\]  |
  fermi\_energy             double                              |Units attribute required. The attribute "scale to atomic units" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).
  smearing\_scheme         |char \[character\_string\_length\]  |
  smearing\_width           double                              |Units attribute required. The attribute "scale to atomic units" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).

-   **number\_of\_electrons**: Number of electrons in the elementary
    cell.
-   **exchange\_functional**: String describing the functional used for
    exchange: names should be taken from the ETSF XC library
    specifications (at present, under construction).
-   **correlation\_functional**: String describing the functional used
    for correlation: Lee Yang Parr or Colle-Salvetti etc\... names
    should be taken from the ETSF XC library specifications.
-   **fermi\_energy**: Fermi energy corresponding to occupation numbers.
-   **smearing\_scheme**: Smearing scheme used for metallic or finite
    temperature occupation numbers = \"gaussian", \"fermi-dirac\",
    \"cold-smearing\", \"methfessel-paxton-n\" for n=1 \... 10.
-   **smearing\_width**: Smearing width used with scheme above.

#### Reciprocal space

  Variables                 |Type (index order as in C)                                          |Notes
  --------------------------|--------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  kinetic\_energy\_cutoff   |double                                                              |Units attribute required. The attribute "scale to atomic units" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).
  kpoint\_grid\_shift       |double \[number\_of\_reduced\_dimensions\]                          |
  kpoint\_grid\_vectors     |double \[number\_of\_vectors\] \[number\_of\_reduced\_dimensions\]  |
  monkhorst\_pack\_folding  |integer \[number\_of\_vectors\]                                     |

-   **kinetic\_energy\_cutoff**: Cutoff used to generate the plane-wave
    basis set.
-   **kpoint\_grid\_vectors**: Basis vectors for kpoint grid if it is
    homogeneous. Given in the coordinates of reciprocal space primitive
    vectors.
-   **kpoint\_grid\_shift**: Shift for offset of grid of kpoints. Used
    with both kpoint\_grid\_vectors and monkhorst\_pack\_folding.
-   **monkhorst\_pack\_folding**: This indicates the "folding" for
    regular kpoint grids (e.g. Monkhorst-Pack Phys. Rev. B 13, 5188
    (1976)). An alternative to kpoint\_grid\_vectors.

### Naming conventions

NetCDF files, that respect the ETSF specifications described in the
present document, should be easily recognized. We suggest to append, in
their names, the string \"-etsf.nc" . The appendix \".nc" is a standard
convention for naming NetCDF files, see:\
<http://www.unidata.ucar.edu/software/netcdf/docs/faq.html#filename> .
Some filesystems are case- insensitive, and this motivates the
lower-case choice. Finally, a dash is to be preferred to an underscore
to allow the files references by a Web search engine.

Specification for files containing crystallographic data
--------------------------------------------------------

### Specification

A ETSF NetCDF file for crystallographic data should contain the
following set of mandatory information:

1.  The three attributes defined in [Mandatory
    attributes]({{< relref "_index.md#Mandatory_attributes" >}})
2.  The following dimensions from [Dimensions that cannot be
    split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}}):
    -   number\_of\_cartesian\_directions
    -   number\_of\_vectors
    -   number\_of\_atoms
    -   number\_of\_atom\_species
    -   number\_of\_symmetry\_operations
3.  The following variables defined in [Atomic structure and symmetry
    operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}}):
    -   primitive\_vectors
    -   reduced\_symmetry\_matrices
    -   reduced\_symmetry\_translations
    -   space\_group
    -   atom\_species
    -   reduced\_atom\_positions
4.  At least one of the following variables defined in [Atomic structure
    and symmetry
    operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}}),
    to specify the kind of atoms:
    -   atomic\_numbers
    -   atom\_species\_names
    -   chemical\_symbols

The use of **atomic\_numbers** is preferred. If **atomic\_numbers** is
not available, **atom\_species\_names** will be preferred over
**chemical\_symbols**. In case more than one such variables are present
in a file, the same order of preference should be followed by the
reading program.

As mentioned in [General
considerations]({{< relref "_index.md#General_considerations" >}}) and [General
specifications]({{< relref "_index.md#General_specifications" >}}), such file might
contain additional information agreed within ETSF, such as any of the
variables specified in [General
specifications]({{< relref "_index.md#General_specifications" >}}). It might even
contain enough information to be declared a ETSF NetCDF file
\"containing the density" or \"containing the wavefunctions", or both.
Such file might also contain additional information specific to the
software that generated the file. It is not expected that this other
software-specific information be used by another software.

It is not expected that the above-mentioned information be distributed
among different files (unlike for density/potential/wavefunction files,
see later).

### Atomic structure and symmetry operations

Variables and attributes to specify the atomic structure and symmetry
operations.

  Variables                        |Type (index order as in C)                                                                                          |Notes
  ---------------------------------|--------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------
  primitive\_vectors               |double \[number\_of\_vectors\]\[number\_of\_cartesian\_directions\]                                                 |By default, given in Bohr.
  reduced\_symmetry\_matrices      |integer \[number\_of\_symmetry\_operations\]\[number\_of\_reduced\_dimensions\]\[number\_of\_reduced\_dimensions\]  |The \"symmorphic\" attribute is needed.
  reduced\_symmetry\_translations  |double \[number\_of\_symmetry\_operations\]\[number\_of\_reduced\_dimensions\]                                      |The \"symmorphic\" attribute is needed.
  space\_group                     |integer                                                                                                             |Between 1 and 232.
  atom\_species                    |integer \[number\_of\_atoms\]                                                                                       |Between 1 and \"number\_of\_atom\_species\".
  reduced\_atom\_positions         |double \[number\_of\_atoms\]\[number\_of\_reduced\_dimensions\]                                                     |
  atomic\_numbers                  |double \[number\_of\_atom\_species\]                                                                                |
  atom\_species\_names             |char \[number\_of\_atom\_species\]\[character\_string\_length\]                                                     |
  chemical\_symbols                |char \[number\_of\_atom\_species\]\[symbol\_length\]                                                                |
  Attributes                       |Type                                                                                                                |Notes
  symmorphic                       |char(80)                                                                                                            |flag-type attribute, see [Flag-like attributes]({{< relref "_index.md#Flag-like_attributes" >}}).

-   **primitive\_vectors** The primitive vectors, expressed in cartesian
    coordinates.
-   Symmetry operations are defined in real space, with reduced
    coordinates. A symmetry operation in real space sends the input
    point r to the output point r\', with

${r'}_{\alpha}^{red} = \sum_{\beta} S^{red}_{\alpha \beta} r^{red}_{\beta} + t^{red}_{\beta}$

The array **reduced\_symmetry\_matrices** contains the matrices S, in
reduced coordinates, while the vector t, in reduced coordinates, is
contained in the array **reduced\_symmetry\_translations** of the same
Table. There might be a confusion between the two dimensions
**number\_of\_reduced\_dimensions** of this variable. In the C ordering,
the last one corresponds to the beta index in the above-mentioned
formula.

The first symmetry operation must always be unity with translation
vector (0,0,0). If all translations are zero, the attribute
**symmorphic** for reduced\_symmetry\_matrices should be set to \"yes\".

-   **space\_group**: Space group number according to international
    tables of crystallography (from 1 to 232).
-   **atom\_species**: Types of each atom in the unit cell. Note that
    the first type of atom has number \"1", and the last type of atom
    has number \"number\_of\_atom\_species".
-   **reduced\_atom\_positions**: Positions of the different atoms in
    the unit cell in relative / reduced coordinates.
-   **atomic\_numbers**: Atomic number for each atom species. If it does
    not refer to an \"usual\" atom (e.g. fractional charge atoms or
    similar), a non-integer number or zero may be used, but it is
    strongly advised then to also specify the atom\_species\_names
    variable.
-   **atom\_species\_names**: Descriptive name for each atom species =
    \"H\" \"Ga\" plus variants like \"Ga-semicore\" \"C-1s-corehole\"
    \"C-sp2\" \"C1\".
-   **chemical\_symbols**: Chemical symbol for each atom species (as in
    periodic table). If not appropriate (fractional charge atoms or
    similar), \"X\" may be used.
-   **symmorphic**: Flag-type attribute (see [Flag-like
    attributes]({{< relref "_index.md#Flag-like_attributes" >}})), needed for the
    variables reduced\_symmetry\_matrices and
    reduced\_symmetry\_translations.

Specification for files containing a density or a potential
-----------------------------------------------------------

### Specification

A ETSF NetCDF file for a density should contain the following set of
mandatory information:

1.  The three attributes defined in [Mandatory
    attributes]({{< relref "_index.md#Mandatory_attributes" >}})
2.  The following dimensions from [Dimensions that cannot be
    split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}}):
    -   number\_of\_cartesian\_directions
    -   number\_of\_vectors
    -   real\_or\_complex\_density and/or real\_or\_complex\_potential
3.  The following dimensions from [Dimensions that can be
    split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}):
    -   number\_of\_components
    -   number\_of\_grid\_points\_vector1
    -   number\_of\_grid\_points\_vector2
    -   number\_of\_grid\_points\_vector3
4.  The primitive vectors of the cell, as defined in [Atomic structure
    and symmetry
    operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}}).
5.  The density or potential, as defined in
    [Density]({{< relref "_index.md#Density" >}}) or [Exchange and
    correlation]({{< relref "_index.md#Exchange_and_correlation" >}}). This variable
    must be the last, in order not to be limited to 4 GB.

As mentioned in [General
considerations]({{< relref "_index.md#General_considerations" >}}) and [General
specifications]({{< relref "_index.md#General_specifications" >}}), such file might
contain additional information agreed within ETSF, such as any of the
variables specified in [General
specifications]({{< relref "_index.md#General_specifications" >}}). It might even
contain enough information to be declared a ETSF NetCDF file
\"containing crystallographic data" or \"containing the wavefunctions",
or both. Such file might also contain additional information specific to
the software that generated the file. It is not expected that this other
software-specific information be used by another software.

A ETSF NetCDF exchange, correlation, or exchange-correlation potential
file should contain at least one variable among the three presented in
[Exchange and correlation]({{< relref "_index.md#Exchange_and_correlation" >}}) in
replacement of the specification of the density. The type and size of
such variables are similar to the one of the density. The other
variables required for a density are also required for a potential file.
Additional ETSF or software-specific information might be added, as
described previously.

The information might distributed among different files, thanks to the
use of splitting of data for variables:

-   number\_of\_components
-   number\_of\_grid\_points\_vector1
-   number\_of\_grid\_points\_vector2
-   number\_of\_grid\_points\_vector3

In case the splitting related to one of these variables is activated,
then the corresponding variables in [Auxiliary variables for
splitting]({{< relref "_index.md#Auxiliary_variables_for_splitting" >}}) must be
defined. Accordingly, the dimensions of the variables in
[Density]({{< relref "_index.md#Density" >}}) and/or [Exchange and
correlation]({{< relref "_index.md#Exchange_and_correlation" >}}) will be changed, to
accommodate only the segment of data effectively contained in the file.

### Density

  Variables  |Type (index order as in C)                                                                                                                                                     |Notes
  -----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  density    |double\[number\_of\_components\]\[number\_of\_grid\_points\_vector3\]\[number\_of\_grid\_points\_vector2\]\[number\_of\_grid\_points\_vector1\]\[real\_or\_complex\_density\]  |This is a pseudo-density. Note in case of PAW, the augmentation contribution is missing. By default, the density is given in atomic units, that is, number of electrons per Bohr^3^. The "units" attribute is required. The attribute "scale\_to\_atomic\_units" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).

A density in such a format (represented on a 3D homogeneous grid) is
suited for the representation of smooth densities, as obtained naturally
from pseudopotential calculations using plane waves.

This specification for a density can also accommodate the response
densities of Density-Functional Perturbation Theory.

### Exchange and correlation

  Variables                         |Type (index order as in C)                                                                                                                                                       |Notes
  ----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  correlation\_potential            |double\[number\_of\_components\]\[number\_of\_grid\_points\_vector3\]\[number\_of\_grid\_points\_vector2\]\[number\_of\_grid\_points\_vector1\]\[real\_or\_complex\_potential\]  |Note in case of PAW, the augmentation contribution is missing. Units attribute required. The attribute \"scale to atomic units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).
  exchange\_potential               |double\[number\_of\_components\]\[number\_of\_grid\_points\_vector3\]\[number\_of\_grid\_points\_vector2\]\[number\_of\_grid\_points\_vector1\]\[real\_or\_complex\_potential\]  |Note in case of PAW, the augmentation contribution is missing. Units attribute required. The attribute \"scale to atomic units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).
  exchange\_correlation\_potential  |double\[number\_of\_components\]\[number\_of\_grid\_points\_vector3\]\[number\_of\_grid\_points\_vector2\]\[number\_of\_grid\_points\_vector1\]\[real\_or\_complex\_potential\]  |Note in case of PAW, the augmentation contribution is missing. Units attribute required. The attribute \"scale to atomic units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).

Specification for files containing the wavefunctions
----------------------------------------------------

### Specification

A ETSF NetCDF file \"containing the wavefunctions\" should contain at
least the information needed to build the density from this file. Also,
since the eigenvalues are intimately linked to eigenfunctions, it is
expected that such a file contain eigenvalues. Of course, files might
contain less information than the one required, but still follow the
naming convention of ETSF. It might also contain more information, of
the kind specified in other tables of the present document.

A ETSF NetCDF file \"containing the wavefunctions\" should contain the
following set of mandatory information:

1.  The three attributes defined in [Mandatory
    attributes]({{< relref "_index.md#Mandatory_attributes" >}})
2.  The following dimensions from [Dimensions that cannot be
    split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}}):
    -   character\_string\_length
    -   number\_of\_cartesian\_directions
    -   number\_of\_vectors
    -   real\_or\_complex\_coefficients and/or
        real\_or\_complex\_wavefunctions
    -   number\_of\_symmetry\_operations
    -   number\_of\_reduced\_dimensions
3.  The following dimensions from [Dimensions that can be
    split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}):
    -   max\_number\_of\_states
    -   number\_of\_kpoints
    -   number\_of\_spins
    -   number\_of\_spinor\_components
4.  In case of a real-space wavefunctions, the following dimensions from
    [Dimensions that can be
    split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}):
    -   number\_of\_grid\_points\_vector1
    -   number\_of\_grid\_points\_vector2
    -   number\_of\_grid\_points\_vector3
5.  In case of a wavefunction given in terms of a basis set, the
    following dimensions from [Dimensions that can be
    split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}):
    -   max\_number\_of\_coefficients
6.  In case of a wavefunction given in terms of a Daubechies wavelet
    basis set, the following dimensions from [Dimensions that can be
    split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}):
    -   max\_number\_of\_basis\_grid\_points
    -   number\_of\_localization\_regions
7.  The primitive vectors of the cell, as defined in [Atomic structure
    and symmetry
    operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
    (variable primitive\_vectors)
8.  The symmetry operations, as defined in [Atomic structure and
    symmetry
    operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
    (given by the variables reduced\_symmetry\_translations and
    reduced\_symmetry\_matrices)
9.  The information related to each kpoint, as defined in
    [K-points]({{< relref "_index.md#K-points" >}}).
10. The information related to each state (including eigenenergies and
    occupation numbers), as defined in [States]({{< relref "_index.md#States" >}}).
11. In case of basis set representation, the information related to the
    basis set, and the variable coefficients\_of\_wavefunctions, as
    defined in [Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}).
12. For basis\_set equal to \"plane\_waves\", the following variable is
    required from [Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}):
    -   reduced\_coordinates\_of\_plane\_waves
13. For basis\_set equal \"daubechies\_wavelets\", the following
    variables are required from
    [Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}):
    -   coordinates\_of\_basis\_grid\_points
    -   number\_of\_coefficients\_per\_grid\_point
14. In case of real-space representation, the variable following
    variable from [Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}):
    -   real\_space\_wavefunctions

As mentioned in [General
considerations]({{< relref "_index.md#General_considerations" >}}) and [General
specifications]({{< relref "_index.md#General_specifications" >}}), such a file might
contain additional information agreed on within ETSF, such as any of the
variables specified in [General
specifications]({{< relref "_index.md#General_specifications" >}}). It might even
contain enough information to be declared a ETSF NetCDF file
\"containing crystallographic data" or \"containing the density", or
both. Such a file might also contain additional information specific to
the software that generated the file. It is not expected that this other
software-specific information be used by another software.

The information might be distributed among different files, thanks to
the use of splitting of data for variables:

-   max\_number\_of\_states
-   number\_of\_kpoints
-   number\_of\_spins

And, either

-   number\_of\_grid\_points\_vector1
-   number\_of\_grid\_points\_vector2
-   number\_of\_grid\_points\_vector3

or

-   max\_number\_of\_coefficients

In case the splitting related to one of these variables is activated,
then the corresponding variables in [Split
wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}) must be defined.
Accordingly, the dimensions of the variables in
[K-points]({{< relref "_index.md#K-points" >}}), [States]({{< relref "_index.md#States" >}}),
[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}), and
[BSE/GW]({{< relref "_index.md#BSE/GW" >}}) might have to be changed, to accommodate
only the segment of data effectively contained in the file.

### K-points

  Variables                          |Type (index order as in C)                                         |Notes
  -----------------------------------|-------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  reduced\_coordinates\_of\_kpoints  |double\[number\_of\_kpoints\] \[number\_of\_reduced\_dimensions\]  |See possible changes for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  kpoint\_weights                    |double\[number\_of\_kpoints\]                                      |See [Construction of the density]({{< relref "_index.md#Construction_of_the_density" >}}). See also possible changes for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).

-   **reduced\_coordinates\_of\_kpoints**: k-point in relative/reduced
    coordinates.
-   **kpoint\_weights**: k-point integration weights. The weights must
    sum to 1. See [Construction of the
    density]({{< relref "_index.md#Construction_of_the_density" >}}).

### States

  Variables           |Type (index order as in C)                                              |Notes
  --------------------|------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  number\_of\_states  |integer\[number\_of\_spins\]\[number\_of\_kpoints\]                     |The attribute \"k\_dependent\" must be defined.
  eigenvalues         |double\[number of spins\]\[number of kpoints\]\[max number of states\]  |The \"units\" attribute is required. The attribute \"scale\_to\_atomic\_units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}). See also possibles changes for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  occupations         |double\[number of spins\]\[number of kpoints\]\[max number of states\]  |See also possibles changes for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  Attributes          |Type                                                                    |Notes
  k\_dependent        |char(80)                                                                |Attribute of number\_of\_states, flag-type, see [Flag-like attributes]({{< relref "_index.md#Flag-like_attributes" >}}).

-   **number\_of\_states**: Number of states for each kpoint, if varying
    (the attribute k\_dependent must be set to yes). Otherwise (the
    attribute k\_dependent must be set to no), might not contain any
    information, the actual number of states being set to
    max\_number\_of\_states.
-   **eigenvalues**: One-particle eigenvalues/eigenenergies. Should be 0
    if unknown.
-   **occupations**: Occupation numbers. Full occupation for
    spin-unpolarized cases (number\_of\_spins = 1 AND
    number\_of\_spinor\_components = 1) is 2, otherwise it is 1. See
    [Construction of the
    density]({{< relref "_index.md#Construction_of_the_density" >}}).
-   **k\_dependent**: Flag-type attribute (see [Flag-like
    attributes]({{< relref "_index.md#Flag-like_attributes" >}})), needed for the
    variables number\_of\_states, number\_of\_coefficients, and
    reduced\_coordinates\_of\_plane\_waves.

### Wavefunctions

  Variables                                   |Type (index order as in C)                                                                                                                                                                                                                                          |Notes
  --------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  basis\_set                                  |char(character string length)                                                                                                                                                                                                                                       |\"plane\_waves\" if a plane-wave basis set is used. \"Daubechies\_wavelets\" if a Daubechies wavelet is used.
  number\_of\_coefficients                    |integer\[number\_of\_kpoints\]                                                                                                                                                                                                                                      |The attribute \"k\_dependent\" must be defined (see [States]({{< relref "_index.md#States" >}})). Possible splitting, see [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  coefficients\_of\_wavefunctions             |double \[number\_of\_spins\]\[number\_of\_kpoints\]\[max\_number\_of\_states\]\[number\_of\_spinor\_components\]\[max\_number\_of\_coefficients\]\[real\_or\_complex\_coefficients\]                                                                                |For both plane-wave basis set and Daubechies wavelet basis set. Normalization for plane waves: 1 per unit cell. See also possible modifications for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}). The attribute used\_time\_reversal\_at\_gamma might be defined.
  reduced\_coordinates\_of\_plane\_waves      |integer\[number\_of\_kpoints\]\[max\_number\_of\_coefficients\]\[number\_of\_reduced\_dimensions\]                                                                                                                                                                  |The attribute \"k\_dependent\" must be defined (see [States]({{< relref "_index.md#States" >}})). See possible modifications for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}). The attribute used\_time\_reversal\_at\_gamma might be defined.
  coordinates\_of\_basis\_grid\_points        |integer\[number\_of\_localization\_regions\]\[max\_number\_of\_basis\_grid\_points\]\[number\_of\_reduced\_dimensions\]                                                                                                                                             |For wavelets.
  number\_of\_coefficients\_per\_grid\_point  |integer\[number\_of\_localization\_regions\]\[max\_number\_of\_basis\_grid\_points\]                                                                                                                                                                                |For wavelets.
  order\_of\_Daubechies\_wavelets             |integer                                                                                                                                                                                                                                                             |For wavelets.
  real\_space\_wavefunctions                  |double\[number\_of\_spins\]\[number\_of\_kpoints\]\[max\_number\_of\_states\]\[number\_of\_spinor\_components\]\[number\_of\_grid\_points\_vector3\]\[number\_of\_grid\_points\_vector2\]\[number\_of\_grid\_points\_vector1\]\[real\_or\_complex\_wavefunctions\]   Normalization: 1 per unit cell. See possible modifications for split files in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  Attributes                                  |Type                                                                                                                                                                                                                                                                |Notes
  used\_time\_reversal\_at\_gamma             |char(80)                                                                                                                                                                                                                                                            |Attribute of reduced coordinates of plane waves and coefficients of wavefunctions flag-type, see [Flag-like attributes]({{< relref "_index.md#Flag-like_attributes" >}}).

-   **basis\_set**:|Type of basis set used if not in a real-space grid.
    At present, either \"plane\_waves" or \"Daubechies\_wavelets".
-   **number\_of\_coefficients**: Number of basis function coefficients
    for each kpoint, if varying (the attribute k\_dependent must be set
    to yes). Otherwise (the attribute k\_dependent must be set to no),
    might not contain any information, the actual number of coefficients
    being set to max\_number\_of\_coefficients.
-   **reduced\_coordinates\_of\_plane\_waves**: Plane-wave G-vectors in
    relative/reduced coordinates. If the attribute k\_dependent is set
    to no, then the dimension \[number\_of\_kpoints\] must be omitted.
    On the other hand, if the attribute used\_time\_reversal\_at\_gamma
    is set to yes (only allowed for the plane wave basis set), then, for
    the Gamma k point - reduced\_coordinates\_of\_kpoints being equal to
    (0 0 0) - the time reversal symmetry has been used to nearly halve
    the number of plane waves, with the coefficients of the wavefunction
    for a particular reciprocal vector being the complex conjugate of
    the coefficients of the wavefunction at minus this reciprocal
    vector. So, apart the origin, the coefficient of only one out of
    each pair of corresponding plane waves ought to be specified. Note
    that in the present version of this specification, spatial
    symmetries should not be used to decrease the number of plane waves.
    Note also that the dimension max\_number\_of\_coefficients actually
    governs the size of reduced\_coordinates\_of\_plane\_waves, so only
    when the gamma kpoint is present alone, will the size of the file
    effectively be reduced by the factor of two.
-   **coefficients\_of\_wavefunctions**: Wavefunction coefficients. The
    wavefunctions must be normalized to 1, i.e. the sum of the absolute
    square of the coefficients of one wavefunction must be 1. See
    [Construction of the
    density]({{< relref "_index.md#Construction_of_the_density" >}}). The attribute
    used\_time\_reversal\_at\_gamma must be used in the same way as for
    the variable reduced\_coordinates\_of\_plane\_waves .
-   **coordinates\_of\_basis\_grid\_points**: For wavelets. Coordinates
    of the grid points where coefficients can be stored. This is used to
    define a real space basis set where a reduced set of points is used.
    This array may be used in conjunction with the variable
    number\_of\_coefficients\_per\_grid\_points.
-   **number\_of\_coefficients\_per\_grid\_points**: For wavelets. This
    array gives the number of coefficients stored on basis set grid
    points. The coordinates of corresponding grid points are given in
    the array coordinates\_of\_basis\_grid\_points.
-   **order\_of\_Daubechies\_wavelets**: For wavelets. This number gives
    the order of the Daubechies wavelet basis, e.g. if
    order\_of\_Daubechies\_wavelets is 14, the Daubechies wavelet are
    made from piecewise polynomial of order 14.
-   **used\_time\_reversal\_at\_gamma**: Flag-type attribute (see
    [Flag-like attributes]({{< relref "_index.md#Flag-like_attributes" >}})), that can
    be used for the variables reduced\_coordinates\_of\_plane\_waves and
    coefficients\_of\_wavefunctions
-   **real\_space\_wavefunctions**: Wavefunction coefficients. Unlike
    for explicit basis set, the wavefunctions must be normalized to 1
    per unit cell, i.e. the sum of the absolute square of the
    coefficients of one wavefunction, for all points in the grid,
    divided by the number of points must be 1. See [Construction of the
    density]({{< relref "_index.md#Construction_of_the_density" >}}). Note that this
    array has a number of dimensions that exceeds the maximum allowed in
    Fortran (that is, seven). This leads to practical problems only if
    the software to read/write this array attempts to read/write it in
    one shot. Our suggestion is instead to read/write sequentially parts
    of this array, e.g. to write the spin up part of it, and then, add
    the spin down. This might be done using Fortran arrays with at most
    seven dimensions.

When the variable basis\_set is set to \"daubechies\_wavelets\", the
basis set is constituted by a reduced set of grid points that can host
one or several coefficients. The following explanation assumes a
two-level resolution but it can be used for other values. In the
two-resolution case, all other quantities than the wavefunctions (as the
density) are usually expressed on the finest grid, i.e. the grid for the
density is twice the grid for the wavefunctions. Since dimensions
number\_of\_grid\_points\_vector\<i\> are used to define the scalar
variables, the coordinates\_of\_basis\_grid\_points must be even numbers
in the two- resolution case. The wavefunctions are expanded in real
space on a non-complete uniform grid. The grid points used for the basis
set are listed in the variable coordinates\_of\_basis\_grid\_points.
Each basis grid point can host one or eight coefficients as stored in
the variable number\_of\_coefficients\_per\_grid\_points. Then, in that
case, the dimension max\_number\_of\_coefficients is the sum over the
basis gridpoint of the values of
number\_of\_coefficients\_per\_grid\_point. To build the wavefunctions
from the values stored in coefficients\_of\_wavefunctions, one must read
for each basis grid point the required number of coefficients. When one
coefficient is given, this means a coefficient for a product of
1-dimensional Daubechies scaling-functions centered on the basis grid
point. When eight values are given, this means eight coefficients for
product of both scaling functions and wavelet functions ($\phi$ denotes
Daubechies scaling functions and $\psi$ Daubechies wavelet functions):

-   $\phi (x) \phi (y) \phi (z)$
-   $\psi (x) \phi (y) \phi (z)$
-   $\phi (x) \psi (y) \phi (z)$
-   $\psi (x) \psi (y) \phi (z)$
-   $\phi (x) \phi (y) \psi (z)$
-   $\psi (x) \phi (y) \psi (z)$
-   $\phi (x) \psi (y) \psi (z)$
-   $\psi (x) \psi (y) \psi (z)$

For a review on wavelets, including the description of Daubechies
wavelets, see, e.g., *Wavelets and Their Application for the Solution of
Partial Differential Equations in Physics*, Presses Polytechniques et
Universitaires Romandes, Lausanne, (1998) by S. Goedecker.

Note that these specification for the wavefunctions can accommodate the
response wavefunctions of Density-Functional Perturbation Theory. On the
contrary, the response eigenenergies (actually a hermitian matrix of
Lagrange multipliers) cannot be accommodated by the \"eigenvalues\"
array of [States]({{< relref "_index.md#States" >}}).

### Split wavefunctions

Different variables see their dimensions modified, in case the file is
split, as described in [Splitting]({{< relref "_index.md#Splitting" >}}) (see
[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
and [Auxiliary variables for
splitting]({{< relref "_index.md#Auxiliary_variables_for_splitting" >}})). In the
following table we have gathered the variables whose dimensions will
change. We have also dimensioned them as if the splitting was done on
all the possible dimensions. This will rarely be the case, but
intermediate situations can easily be deduced from the data gathered in
the table.

  Variables                               |Type (index order as in C)                                                                                                                                                                                                                                                                      |Notes
  ----------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  reduced\_coordinates\_of\_kpoints       |double\[my\_number\_of\_kpoints\]\[number\_of\_reduced\_dimensions\]                                                                                                                                                                                                                            |
  number\_of\_coefficients                |integer\[my\_number\_of\_kpoints\]                                                                                                                                                                                                                                                              |
  kpoint\_weights                         |double\[my\_number\_of\_kpoints\]                                                                                                                                                                                                                                                               |
  occupations                             |double\[my\_number\_of\_spins\]\[my\_number\_of\_kpoints\]\[my\_max\_number\_of\_states\]                                                                                                                                                                                                       |
  eigenvalues                             |double\[my\_number\_of\_spins\]\[my\_number\_of\_kpoints\]\[my\_max\_number\_of\_states\]                                                                                                                                                                                                       |The \"units\" attribute is required. The attribute \"scale\_to\_atomic\_units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}).
  real\_space\_wavefunctions              |double \[my\_number\_of\_spins\]\[my\_number\_of\_kpoints\]\[my\_max number\_of\_states\]\[my\_number\_of\_spinor\_components\]\[my\_number\_of\_grid\_points\_vector1\]\[my\_number\_of\_grid\_points\_vector2\]\[my\_number\_of\_grid\_points\_vector3\]\[real\_or\_complex\_wavefunctions\]   |
  coefficients\_of\_wavefunctions         |double \[my\_number\_of\_spins\]\[my\_number\_of\_kpoints\]\[my\_max\_number\_of\_states\]\[my\_number\_of\_spinor\_components\]\[my\_max\_number\_of\_coefficients\]\[real\_or\_complex\_coefficients\]   |                                                                                     
  reduced\_coordinates\_of\_plane\_waves  |integer\[my\_number\_of\_kpoints\]\[number\_of\_reduced\_dimensions\]                                                |                                                                                                                                                                           

### BSE/GW

The variables mentioned in this table are optional. They have been
introduced in the present specification in prevision of use by some
GW/BSE softwares, and might be subject to (heavy?) revisions in future
versions of the specification.

  Dimensions                         |Type                                                                                                                                                             |Notes
  -----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  max\_number\_of\_angular\_momenta  |integer                                                                                                                                                          |
  max\_number\_of\_projectors        |integer                                                                                                                                                          |
  Variables                          |Type (index order as in C)                                                 |                                                                                     |Notes
  gw\_corrections                    |double \[number\_of\_spins\]\[number\_of\_kpoints\]\[max\_number\_of\_states\]\[real\_or\_complex\_gw\_corrections\]                                             |The \"units\" attribute is required. The attribute \"scale\_to\_atomic\_units\" might also be mandatory, see [Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}}). See also possibles changes for split files, as in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  kb\_formfactor\_sign               |integer\[number\_of\_atom\_species\]\[max\_number\_of\_angular\_momenta\]\[max\_number\_of\_projectors\]                                                         |
  kb\_formfactors                    |double\[number\_of\_atom\_species\]\[max\_number\_of\_angular\_momenta\]\[max\_number\_of\_projectors\]\[number\_of\_kpoints\]\[max\_number\_of\_coefficients\]  |Possibles changes for split files, as in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).
  kb\_formfactor\_derivative         |double\[number\_of\_atom\_species\]\[max\_number\_of\_angular\_momenta\]\[max\_number\_of\_projectors\]\[number\_of\_kpoints\]\[max\_number\_of\_coefficients\]  |Possibles changes for split files, as in [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}}).

-   **gw\_corrections**: GW-corrections to one-particle eigenvalues (see
    [Split wavefunctions]({{< relref "_index.md#Split_wavefunctions" >}})). Imaginary
    part (originating from the non-hermiticity) is optional. Should be 0
    if unknown.
-   **max\_number\_of\_angular\_momenta**: The maximum number of angular
    momenta to be considered for non-local Kleinman-Bylander separable
    norm-conserving pseudopotentials. If there is no non-local part, set
    it to 0. If the s channel is the highest angular momentum channel
    over all atomic species, then set it to 1. If the p channel (resp. d
    or f) is the highest, set it to 2 (resp. 3 or 4).
-   **max\_number\_of\_projectors**: The maximum number of projectors
    for non-local Kleinman-Bylander separable norm-conserving
    pseudopotentials, over all angular momenta and all atomic species.
    If there is no non-local part, set it to 0. Most separable norm-
    conserving pseudopotentials have only one projector per angular
    momentum channel.
-   **kb\_formfactor\_sign**: An array of integers whose value depend on
    the specific atomic species, angular momentum, and projector. It can
    have three values: when 0, it means that there is no projector
    defined for that channel. When +1 or -1, it gives the sign of the
    Kleinman-Bylander projector for that channel.
-   **kb\_formfactors**: Kleinman-Bylander form factors in reciprocal
    space.
-   **kb\_formfactor\_derivatives**: Kleinman-Bylander form factors
    derivatives in reciprocal space.

On the Kleinman-Bylander form factors, we note that one can always write
the non-local part of Kleinman-Bylander pseudopotential (reciprocal
space) in the following way:

$v^{KB}_{nonloc} (\vec{K},\vec{K'}) = \sum_s \left[ \sum_{a(s)} e^{-i(\vec{K}-\vec{K'})\vec{\tau_a}}\right]
            \left[ \sum_{lp} P_l(\hat{K} \cdot \hat{K'}) F^{\star}_{slp}(K) S_{slp} F_{slp}(K') \right]$

with $\vec{K} = \vec{k} + \vec{G}$ , $\vec{k}$ is one of the kpoints
(see [Exchange and correlation]({{< relref "_index.md#Exchange_and_correlation" >}})),
$\vec{G}$ is a vector of the reciprocal lattice, the list of reduced
coordinates of which can be found in the variable
reduced\_coordinates\_of\_plane\_waves of
[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}}). $K$ is the module of
$\vec{K}$ and $\hat{K}$ its direction. $\vec{\tau_a}$ is the atomic
position of atom $a$ belonging to species $s$. $P_l (x)$ is the Legendre
polynomial of order $l$. $F_{slp} (K)$ is the Kleinman-Bylander form
factor for species $s$, angular polynomial of order $l$ , and number of
projector $p$ . $S_{slp}$ is the sign of the dyadic product
$F_{slp}^{\star}(K) F_{slp}(K')$. The sum on $a(s)$ runs over all atoms
of atomic species $s$, $l$ runs over all the pseudopotential angular
momentum components of the atomic species $s$, and $p$ runs over the
number of projectors allowed for a specific angular momentum channel of
atomic species $s$. The additional variable kb\_formfactor\_derivative
is equal to $d F_{slp}(K) / dK$.

### Construction of the density

Supposing $\rho_{n, k} (r)$ to be the partial density at point r (in
real space, using reduced coordinates) due to band n at k-point k (in
reciprocal space, using reduced coordinates), then the full density at
point is obtained thanks to:

$\rho(r^{red}\alpha) = \sum_{s \in sym} \sum_k w_k \sum_n f_{n,k} \rho_{n, k} \left( S^{red}_{s,\alpha \beta} (r^{red}_\beta-t^{red}_{s,\beta}) \right)$,

where $w_k$ is contained in the array \"kpoint\_weights" of
[K-points]({{< relref "_index.md#K-points" >}}), and $f_{n, k}$ is contained in the
array \"occupations" of [States]({{< relref "_index.md#States" >}}). This relation
generalizes to the collinear spin-polarized case, as well as the
non-collinear case by taking into account the \"number\_of\_components\"
defined in [Dimensions that can be
split]({{< relref "_index.md#Dimensions_that_can_be_split" >}}), and the direction of
the magnetization vector.

Appendix A: Some information on the NetCDF size limitation
----------------------------------------------------------

To summarize:

-   The 2GB limit is firstly a FILE-SIZE limit of operating systems on
    32-bits machine (and some non-updated 64-bits
    old-operating-systems). And this cannot be overcome, even splitting
    wavefunctions into nbands\*nkpoints variables.
-   Assuming your machine can store \>2GB files, the NetCDF has in
    general a limit of 4GB. BUT even with the actual version you can
    store in NetCDF at least one variable (the last) up to Terabytes,
    and probably in future this will be extended to also the non last
    variables.

### NetCDF 64-bit Offset Format Limitations

Although the 64-bit offset format allows the creation of much larger
NetCDF files than was possible with the classic format, there are still
some restrictions on the size of variables. It is important to note that
without Large File Support (LFS) in the operating system, it is
impossible to create any file larger than 2 GBytes. Assuming an
operating system with LFS, the following restrictions apply to the
NetCDF 64-bit offset format:

-   No fixed-size variable can require more than 2^32^ - 4 bytes (i.e.
    4GB - 4 bytes, or 4,294,967,292 bytes) of storage for its data,
    unless it is the last fixed-size variable and there are no record
    variables. When there are no record variables, the last fixed-size
    variable can be any size supported by the file system, e.g.
    terabytes.
-   A 64-bit offset format NetCDF file can have up to 2^32^ - 1 fixed
    sized variables, each under 4GB in size. If there are no record
    variables in the file the last fixed variable can be any size. No
    record variable can require more than 2^32^ - 4 bytes of storage for
    each record's worth of data, unless it is the last record variable.
    A 64-bit offset format NetCDF file can have up to 2^32^ - 1 records,
    of up to 2^32^ - 1 variables, as long as the size of one record's
    data for each record variable except the last is less than 4 GB - 4.

Note also that all NetCDF variables and records are padded to 4-byte
boundaries.

Appendix B: List of things under debate
---------------------------------------

-   Should formulate specification for other susceptibilities (spin,
    frequency, real or reciprocal representation), electron-phonon,
    dynamical matrices
-   Tolerances / treshhold for equality of two double numbers (e.g. k
    points, when given explicitely) ; one might define a tolerance in
    the specif, or define some tolerance variables ?!
-   Specification should be clarified about Monkhorst-Pack sampling in
    case where the original article refers to conventional reciprocal
    lattice, and not primitive one
-   Should discuss symmetries in case of magnetization (collinear and
    non-collinear).
-   Should plan PAW / USPP generalisation , perhaps LAPW ?
-   Should debate about the interest of a pseudopotential specif -
    perhaps PAW/USPP atomic data.

Appendix C. List of ETSF NetCDF agreed names
--------------------------------------------

Here is a list of all the names of agreed variables, attributes, and
dimensions names, in alphabetical order.

Note: all the variables/dimensions beginning with \"my\_\" refer to
split files, and are explained in [Auxiliary dimensions for
splitting]({{< relref "_index.md#Auxiliary_dimensions_for_splitting" >}}) and
[Auxiliary variables for
splitting]({{< relref "_index.md#Auxiliary_variables_for_splitting" >}}).

  Name                                        |Type              |Table
  --------------------------------------------|------------------|--------------------------------------------------------------------------------------------------
  atom\_species                               |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  atom\_species\_names                        |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  atomic\_numbers                             |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  basis\_set                                  |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  character\_string\_length                   |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  chemical\_symbols                           |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  coefficients\_of\_wavefunctions             |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  Conventions                                 |Global attribute  |[Mandatory attributes]({{< relref "_index.md#Mandatory_attributes" >}})
  coordinates\_of\_basis\_grid\_points        |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  correlation\_functional                     |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  correlation\_potential                      |Variable          |[Exchange and correlation]({{< relref "_index.md#Exchange_and_correlation" >}})
  density                                     |Variable          |[Density]({{< relref "_index.md#Density" >}})
  eigenvalues                                 |Variable          |[States]({{< relref "_index.md#States" >}})
  exchange\_correlation\_potential            |Variable          |[Exchange and correlation]({{< relref "_index.md#Exchange_and_correlation" >}})
  exchange\_functional                        |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  exchange\_potential                         |Variable          |[Exchange and correlation]({{< relref "_index.md#Exchange_and_correlation" >}})
  fermi\_energy                               |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  file\_format                                |Global attribute  |[Mandatory attributes]({{< relref "_index.md#Mandatory_attributes" >}})
  file\_format\_version                       |Global attribute  |[Mandatory attributes]({{< relref "_index.md#Mandatory_attributes" >}})
  gw\_corrections                             |Variable          |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  history                                     |Global attribute  |[Optional attributes]({{< relref "_index.md#Optional_attributes" >}})
  k\_dependent                                |Attribute         |[States]({{< relref "_index.md#States" >}})
  kb\_formfactor\_sign                        |Variable          |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  kb\_formfactors                             |Variable          |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  kb\_formfactor\_derivative                  |Variable          |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  kinetic\_energy\_cutoff                     |Variable          |[Reciprocal space]({{< relref "_index.md#Reciprocal_space" >}})
  kpoint\_grid\_shift                         |Variable          |[Reciprocal space]({{< relref "_index.md#Reciprocal_space" >}})
  kpoint\_grid\_vectors                       |Variable          |[Reciprocal space]({{< relref "_index.md#Reciprocal_space" >}})
  kpoints\_weights                            |Variable          |[K-points]({{< relref "_index.md#K-points" >}})
  max\_number\_of\_angular\_momenta           |Dimension         |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  max\_number\_of\_basis\_grid\_points        |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  max\_number\_of\_coefficients               |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  max\_number\_of\_projectors                 |Dimension         |[BSE/GW]({{< relref "_index.md#BSE/GW" >}})
  max\_number\_of\_states                     |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  monkhorst\_pack\_folding                    |Variable          |[Reciprocal space]({{< relref "_index.md#Reciprocal_space" >}})
  number\_of\_atoms                           |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  number\_of\_atom\_species                   |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  number\_of\_cartesian\_directions           |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  number\_of\_coefficients                    |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  number\_of\_coefficients\_per\_grid\_point  |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  number\_of\_components                      |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_electrons                       |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  number\_of\_grid\_points\_vector1           |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_grid\_points\_vector2           |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_grid\_points\_vector3           |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_kpoints                         |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_localization\_regions           |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_reduced\_dimensions             |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  number\_of\_spinor\_components              |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_spins                           |Dimension         |[Dimensions that can be split]({{< relref "_index.md#Dimensions_that_can_be_split" >}})
  number\_of\_states                          |Variable          |[States]({{< relref "_index.md#States" >}})
  number\_of\_symmetry\_operations            |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  number\_of\_vectors                         |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  occupations                                 |Variable          |[States]({{< relref "_index.md#States" >}})
  order\_of\_Daubechies\_wavelets             |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  primitive\_vectors                          |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  pseudopotential\_types                      |Variable          |[Atomic information]({{< relref "_index.md#Atomic_information" >}})
  real\_or\_complex\_coefficients             |Variable          |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  real\_or\_complex\_density                  |Variable          |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  real\_or\_complex\_gw\_corrections          |Variable          |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  real\_or\_complex\_potential                |Variable          |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  real\_or\_complex\_wavefunctions            |Variable          |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  real\_space\_wavefunctions                  |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  reduced\_atom\_positions                    |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  reduced\_coordinates\_of\_kpoints           |Variable          |[K-points]({{< relref "_index.md#K-points" >}})
  reduced\_coordinates\_of\_plane\_waves      |Variable          |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  reduced\_symmetry\_matrices                 |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  reduced\_symmetry\_translations             |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  scale\_to\_atomic\_units                    |Attribute         |[Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}})
  smearing\_scheme                            |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  smearing\_width                             |Variable          |[Electronic structure]({{< relref "_index.md#Electronic_structure" >}})
  space\_group                                |Variable          |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  symbol\_length                              |Dimension         |[Dimensions that cannot be split]({{< relref "_index.md#Dimensions_that_cannot_be_split" >}})
  symmorphic                                  |Attribute         |[Atomic structure and symmetry operations]({{< relref "_index.md#Atomic_structure_and_symmetry_operations" >}})
  title                                       |Global attribute  |[Optional attributes]({{< relref "_index.md#Optional_attributes" >}})
  units                                       |Attribute         |[Generic attributes of variables]({{< relref "_index.md#Generic_attributes_of_variables" >}})
  used\_time\_reversal\_at\_gamma             |Attribute         |[Wavefunctions]({{< relref "_index.md#Wavefunctions" >}})
  valence\_charges                            |Variable          |[Atomic information]({{< relref "_index.md#Atomic_information" >}})
