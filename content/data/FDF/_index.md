+++
title = "FDF"
date =  2020-05-12T21:36:07+01:00
weight = 15
tags = ['ESL entries','Data standards','I/O']
+++


Authors
-------

- [Jose M. Soler](http://www.uam.es/departamentos/ciencias/fismateriac/pagesmem/jose_soler.html), [U. Autónoma Madrid, Spain](http://www.uam.es)
- [Alberto García](http://departments.icmab.es/leem/alberto), [ICMAB-CSIC, Barcelona, Spain](http://www.icmab.es),
- [Raúl de la Cruz](http://www.bsc.es/about-bsc/staff-directory/de-la-cruz-martinez-raul), [BSC, Barcelona, Spain](http://www.bsc.es)

Functionalities
----------------

- Input file parser
- FDF - Flexible Data Format

Version
-------

0.6.6, September 2007

Description
-----------

Input lines are parsed and a token list is created. A token is:

-   Any number of characters enclosed in a matching pair of quotes
    (`"`,`'`,`` ` ``). Embedded strings are not considered tokens. The
    quotes are removed by the parsing routine.
-   A string of characters from the set \[`A`-`Z`,`a`-`z`,`0`-`9`\] of
    alphanumeric characters plus `$%&@_.-*/^+~` (and any adjacent
    unmatched quotes, as in apostrophes)
-   The character `<` by itself.

Examples:

`MeshCutoff < "File.fdf"` → 3 tokens: `MeshCutoff`, `<`, `File.fdf`

`System_Name Devil's staircase` → 3 tokens: `System_Name`, `Devil's`,
`staircase`

`%block AtomicCoordinates %dump < coords.dat` → 5 tokens

The characters `#`, `;`, and `!` signal the beginning of a comment. Any
remaining characters on the line are discarded.

Labels and directives (`%block`, `%include`, `%endblock`) are
case-insensitive. Labels are also punctuation-insensitive, by which it
is understood that the characters `.`, `_`, and `-` are removed before
any comparison is made.

``` {.text}
Label   [ Value ]
```

This line assigns an optional `Value` to `Label`. Value could be a token
or a sequence of tokens not including `<`. \'Numerical\' routines such
as `fdf_integer` will only use the first token, but `fdf_string` will
return the whole list.

``` {.text}
%block Label
  ...
  ...
  ...
# Optional comments
%endblock [ Label ]
```

This construct specifies a block of data that is to be read by the
calling program after a call to `fdf_block`. The contents of the block
are dumped to `fdf_out`. `%block` and `%endblock` must be the first
tokens in their respective lines.

``` {.text}
%block Label  < Filename  [ %dump ]
```

Opens Filename in order to read the block. If `%dump` appears at the
*end* of the line, the contents of Filename are dumped to `fdf_out`.

``` {.text}
%include  Filename  # Comments
```

Opens `Filename` and continues reading from it.

``` {.text}
Label   < Filename   # Comments
```

Opens `Filename` and continues reading from it, ONLY if searching for
`Label`.

There is a maximum number of files that can be opened at the same time
for FDF processing.

Debugging and logging {#debugging_and_logging}
---------------------

By default, all the FDF requests are logged, printing the final value
extracted (if it is the default value, it is identified as such). In MPI
operation, only the master node does the logging, unless the
`output-level` is set to a value ≥`2`. This can be done directly in the
FDF file:

``` {.text}
fdf-output 2  # Turn on logging for all nodes
fdf-output 0  # Turn off logging completely
```

No debugging is done, unless the `debug-level` is set to a value \>`0`.
The most meaningful way to use this feature is to set the debugging
level before the FDF data structures are built. This can be achieved by
calling the routine `fdf_setdebug` with the appropriate level and file
name *before* the call to `fdf_init`. This gives full control over the
behavior.

Alternatively, if the library is compiled with the pre-processor symbol
`FDF_DEBUG` defined, it will set the debug level to `2` (exhaustive) for
all nodes in the system. (It will also set the output level to `2` (all
nodes)).

If the debugging level (≥`2`) is only specified in the FDF file itself,
the library will provide a print-out of the final data structure
generated (in all nodes).
