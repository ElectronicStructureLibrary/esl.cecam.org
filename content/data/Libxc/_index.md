+++
title = "Libxc IDs"
date =  2020-05-12T21:36:07+01:00
weight = 35
tags = ["Data standards","ESL Entries"]
+++

Because of the large number of functionals available in
[Libxc]({{< ref "/esl/libxc/_index.md" >}}), the way how it identifies functionals has
become a *de facto* standard, as some codes and libraries also started
using it.

[Libxc]({{< ref "/esl/libxc/_index.md" >}}) has two ways of identifying a functional:
using a string, which is called the functional name, or using an integer
number, which is called the functional number.

Libxc functional number
-----------------------

The functional number is an integer that is set at the time of the
implementation of the corresponding functional in the library and the
only rules to select it are the following:

-   it must be comprised between 1 and 999;
-   it must be unique.

Once set, this number never changes and remains the same over all
[Libxc]({{< ref "/esl/libxc/_index.md" >}}) releases. Therefore, this is the preferred way
of identifying a functional if backward compatibility is required. The
disadvantage of using it is that it is not human-friendly, as, by
itself, it conveys no information whatsoever about the functional.

Libxc functional name
---------------------

The functional name is intended to be a human-readable string and,
besides identifying the functional, it contains some information about
it, like the family and kind. Note that this name might change between
[Libxc]({{< ref "/esl/libxc/_index.md" >}}) releases.

All the names have the following structure:
*XC\_(FAMILY)\_(KIND)\_(SHORTNAME)*, with:

-   *(FAMILY)*: the functional family. Can be **LDA**, **GGA**,
    **MGGA**, **HYB\_GGA**, or **HYB\_MGGA**.
-   *(KIND)*: the functional kind. Can be **C** (correlation), **X**
    (exchange), **XC** (exchange and correlation), or **K** (kinetic
    energy).
-   *(SHORTNAME)*: the functional shortname. This is a shortname chosen
    at the time of the implementation of the corresponding functional in
    the library and is usually an acronym, like **PBE**, which is the
    acronym of the functional\'s authors last name.
