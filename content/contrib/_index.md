+++
title = "Contributing"
date = 2020-05-12T21:03:56+01:00
weight = 500
+++

Contributions from the community are encouraged. If you have a piece of
software or a proposal for an API or a data standard that you think
would be a good addition to the ESL, here are the steps to follow:

1.  [Login to Gitlab](https://gitlab.com/electronicstructurelibrary)
2.  Create a new page on the [ESL website repository](https://gitlab.com/electronicstructurelibrary/esl.cecam.org)
3.  Good luck!

A bit more information\...
--------------------------

First of all, read [About ESL]({{< ref "/about/_index.md" >}}) for an introduction
to the possible categories for ESL entries. Have a look around the
existing entries to get a feel for how the division in categories works
in practice. Also look at our dummy pages for
[Software]({{< ref "/esl/_index.md" >}}) and Functionalities.

Then, ask yourself a few questions:

-   What kind of entry is right for my contribution? What categories
    should it contain?
-   Do I want to create and maintain the wiki entry personally? This is
    the preferred option, and is not a difficult task. If not, [contact
    us]({{< ref "/contact.en.md" >}}) to find out if one of our contributors is
    interested in doing so.
-   If the entry contains a piece of software, how will the code be
    maintained and linked to the ESL? You can opt for an external
    repository/website, a mirrored repository, or an internal
    development.

We remind you to pay special attention to the category tags at the end
of your page. All entries should contain at least two mandatory tags:

-   `ESL entries`
-   All that apply from: `Functionalities`, `Data standards`,
    `ESL Software`, `Other Software`

You will see that existing entries also contain other tags, categorising
them into themes. For the moment, these are maintained by the [ESL
Curating Team]({{< ref "/credits.en.md" >}}), and so you do
not need to worry about adding them.

If what you are doing involves more than documenting in the wiki (e.g.
coding for ESL, or linking something from ESL into some program), or you
are embarked in a significant documentation project (e.g. data
standards), please, create or edit the `Current developments` section in
the corresponding wiki page.

Finally, reward yourself for all the hard work by adding your name to
the [Credits]({{< ref "/credits.en.md#Contributors" >}}) page.
