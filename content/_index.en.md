---
title: "Welcome to The Electronic Structure Library"
---

# Welcome to The Electronic Structure Library

{{% notice info %}}
To join our community on Zulip, please [follow this sign-in link](https://esl-cecam.zulipchat.com/join/uvatdjuxwzytg5sgwv3ojxsq/).
{{% /notice %}}

## *A repository and library of routines for electronic structure programs*

This is a community-maintained library of software of use for electronic structure simulations. It is an extended library that can be employed by everyone for building their own packages and projects. The library components, the ESL bundle and pointers to documentation of the different subpackages and libraries can be found in the development sites:

{{% notice tip %}}
Electronic Structure Library at [gitlab.com](https://gitlab.com/ElectronicStructureLibrary) and/or [github.com](https://github.com/ElectronicStructureLibrary). Please, note that most projects reside on GitLab, having a mirror on GitHub, while a few projects are hosted on GitHub and are mirrored on GitLab. This is mentioned in the respective description of the projects.
{{% /notice %}}

The ambition of the ESL is to segregate layers of functionality within modules which are general, standardised and efficient. In this way, new ideas, and new science, can be coded by scientists without needing to rewrite functionalities that are already well-established, and without needing to know more software engineering than science. In other words, we want to separate the coding effort for cutting-edge research from the software infrastructure it rests on top of, which needs maintaining and rewriting at every step of the hardware race.
