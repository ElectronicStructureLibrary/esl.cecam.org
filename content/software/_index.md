+++
title = "ESL Software"
date = 2020-05-12T21:22:23+01:00
weight = 20
+++

ESL software is either software born and developed within the ESL initiative or software distributed under the ESL brand with the agreement of its authors.

The entries listed below contain a link to a particular piece of ESL software and its description.
They may also - but do not need to - contain other descriptions in them, such as that of the
algorithm and programming interface used, for instance, and/or the description of the
functionality.

