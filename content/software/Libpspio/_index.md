+++
title = "Libpspio"
date =  2020-05-12T21:28:40+01:00
weight = 35
tags = ['ESL entries', 'ESL Software', 'I/O']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| MPLv2 | [PSPIO team](https://gitlab.com/ElectronicStructureLibrary/libpspio/-/project_members) | [Gitlab](https://gitlab.com/ElectronicStructureLibrary/libpspio/-/releases) |


Libpspio is a pseudopotentials I/O
library for Density-Functional Theory (DFT) calculations. It can both
read and write pseudopotential data, which makes it suitable for use
with pseudopotential generators and electronic structure codes. Its main
objective is to let any DFT code access or produce pseudopotential
information without having to care about file formats. In order to
maximize its relevance, Libpspio is fully code-agnostic and implements
as many formats and variants as possible. Libpspio is a valuable
alternative to most error-prone homemade implementations and is helpful
in improving file format specifications. In order to enhance its
portability and facilitate the quality checking of its source code,
Libpspio has been written in C. The widespread use of Fortran within the
scientific community has however been kept in mind, which is why
Libpspio provides Fortran bindings to most of its API.

Installation
------------

### Prerequisites

{{% notice warning %}}
There is no stable release of Libpspio yet.
Current versions are mostly stable but still lack support for some file
formats.
{{% /notice %}}

From version 1.0, users of Libpspio will just have to make sure that
they have the [Libxml2 XML parsing library](http://xmlsoft.org/)
installed on their platform, which is the case of most existing
Unix-like systems. They will be able to build Libpspio out of the box
without having to install anything else. Those who need to build Libxml2
its source can refer to the corresponding [build
instructions](http://xmlsoft.org/FAQ.html).

### Instructions

On how to install, link, etc.

### Tests

Libpspio contains several [unit
tests](https://wikipedia.org/wiki/Unit_testing) that can be used to check the
compilation and to perform [regression
testing](https://wikipedia.org/wiki/Regression_testing). These tests can be
executed by doing:

`$ make check`

Programming interface
---------------------

Libpspio is made of three component categories:

-   a data structure to store the pseudopotential data in a
    format-independent way;
-   a set of routines and functions to access and modify this data
    structure;
-   a set of error-handling routines and their associated internal
    sturctures.

A complete list of routines and types provided by the library can be
found in the Doxygen documentation:

-   [Development
    version](https://gitlab.com/ElectronicStructureLibrary/libpspio/-/tree/develop).

Using Libpspio
--------------

The following diagram describes a typical way for a DFT code to get
pseudopotential data:

![](Libpspio-workflow.png "Libpspio-workflow.png")

Libpspio provides function calls for each of these steps, that is:

-   determining which kind of information is present in the file;
-   parsing the file and extracting the relevant data;

\...

Current developments
--------------------

-   The library is being augmented to finalise the coverage of most
    formats for norm-conserving pseudopotentials, and is now being
    expanded to PAW and ultrasoft formats.

-   The library is being interfaced with Abinit, Octopus, and APE to
    test and fine tune the interface.

Future developments
-------------------

Future developments can be consulted on the [Gitlab Project
Page](https://gitlab.com/ElectronicStructureLibrary/libpspio).
