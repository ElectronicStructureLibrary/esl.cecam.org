+++
title = "ELSI"
date =  2020-05-12T21:20:47+01:00
weight = 5
tags = ['ESL entries', 'ESL Software', 'Solvers', 'Sparse matrices', 'Localized orbitals']
+++

| License | Authors | Download | Documentation |
|:--------|:--------|:---------|:--------------|
| Revised BSD | [ELSI Team](http://www.elsi-interchange.org/webpage/contacts.html) | [ELSI Web Site](http://www.elsi-interchange.org/) | [Manual](https://wordpress.elsi-interchange.org/wp-content/uploads/2020/02/elsi-manual-2.5.0.pdf) |

### ELSI: **EL**ectronic **S**tructure **I**nfrastructure ###

[ELSI](https://wordpress.elsi-interchange.org/) is a *software
bundle* and *unified interface* for methods that solve or circumvent
eigenvalue problems in electronic structure theory -- for example, in
the self-consistent field cycle of density-functional theory, but also
elsewhere. An interface to BSEpack to solve the Bethe-Salpeter Equation
is also included.

In essence, ELSI will allow an electronic structure
code to run its eigenvalue and/or density matrix solutions through a single
interface, leaving the details of handling the different libraries that are
actually used to solve the problem to ELSI. Thus, switching between (say)
Lapack, ELPA, MAGMA, or an O(N) method becomes much simpler (no more need to
write customized interfaces for each of them).

The [ELSI website](https://wordpress.elsi-interchange.org/) has more information.
The actual source repository of ELSI can be found at [gitlab.com/elsi_project](https://gitlab.com/elsi_project).

The eigensolvers and density matrix solvers supported in ELSI include:

[ELPA](http://elpa.mpcdf.mpg.de/): The massively parallel dense eigensolver ELPA facilitates the solution of symmetric or Hermitian eigenproblems on high-performance computers. It features an efficient two-stage tridiagonalization method which is better suited for parallel computing than the conventional one-stage method.

[LAPACK](https://www.netlib.org/lapack): LAPACK provides routines for solving linear systems, least squares problems, eigenvalue problems, and singular value problems. In order to promote high efficiency on present-day computers, LAPACK routines are written to exploit BLAS, particularly level-3 BLAS, as much as possible. In ELSI, the tridiagonalization and the corresponding back-transformation routines in LAPACK are combined with the efficient divide-and-conquer tridiagonal solver in ELPA.

[MAGMA](https://icl.utk.edu/magma): The MAGMA project aims to develop a dense linear algebra framework for heterogeneous architectures consisting of manycore and GPU systems. MAGMA incorporates the latest advances in synchronization-avoiding and communication-avoiding algorithms, and uses a hybridization methodology where algorithms are split into tasks of varying granularity and their execution scheduled over the available hardware components.

[libOMM](https://esl.cecam.org/software/libomm/): The orbital minimization method (OMM) bypasses the explicit solution of the Kohn-Sham eigenproblem by efficient iterative algorithms that directly minimize an unconstrained energy functional using a set of auxiliary Wannier functions. The Wannier functions are defined on the occupied subspace of the system, reducing the size of the problem. The density matrix is then obtained directly, without calculating the Kohn-Sham orbitals.

[PEXSI](http://pexsi.org/): PEXSI is a Fermi operator expansion (FOE) based method which expands the density matrix in terms of a linear combination of a small number of rational functions (pole expansion). Evaluation of these rational functions exploits the sparsity of the Hamiltonian and overlap matrices using selected inversion to enable scaling to 100,000+ of MPI tasks for calculation of the electron density, energy, and forces in electronic structure calculations.

[EigenExa](http://www.r-ccs.riken.jp/labs/lpnctrt/en/projects/eigenexa): The EigenExa library consists of two massively parallel implementations of direct, dense eigensolver. Its eigen_sx method features an efficient transformation from full to pentadiagonal matrix. Eigenvalues and eigenvectors of the pentadiagonal matrix are directly solved with a divide-and-conquer algorithm. This method is particularly efficient when a large part of the eigenspectrum is of interest.

[SLEPc-SIPs](http://slepc.upv.es/): SLEPc-SIPs is a parallel sparse eigensolver for real symmetric generalized eigenvalue problems. It implements a distributed spectrum slicing method and it is currently available through the SLEPc library built on top of the PETSc framework.

[NTPoly](https://william-dawson.github.io/NTPoly): NTPoly is a massively parallel library for computing the functions of sparse, symmetric matrices based on polynomial expansions. For sufficiently sparse matrices, most of the matrix functions can be computed in linear time. Distributed memory parallelization is based on a communication avoiding sparse matrix multiplication algorithm. Various density matrix purification algorithms that compute the density matrix as a function of the Hamiltonian matrix are implemented in NTPoly.

[BSEPACK](https://sites.google.com/a/lbl.gov/bsepack): BSEPACK is a parallel ScaLAPACK-style library for solving the Bethe-Salpeter eigenvalue problem on distributed-memory high-performance computers.


