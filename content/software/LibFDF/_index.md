+++
title = "LibFDF"
date =  2020-05-12T21:28:40+01:00
weight = 10
tags = ['ESL entries', 'ESL Software', 'I/O', 'Functionalities']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| BSD     | [Jose M. Soler](http://www.uam.es/departamentos/ciencias/fismateriac/pagesmem/jose_soler.html), [Alberto García](http://departments.icmab.es/leem/alberto), [Raúl de la Cruz](http://www.bsc.es/about-bsc/staff-directory/de-la-cruz-martinez-raul) | [Gitlab](https://gitlab.com/siesta-project/libraries/libfdf/-/releases) |

FDF (Flexible Data Format) is an input file
parser that offers an easy, transferable
and practical way for a Fortran program to read its input. It is text
(ASCII) based, and conceived for small data (input parameters). Every
input piece of data is introduced in a line of an input file (which can
be standard input) by writing a name-value pair, that is, a name
characterising the data, and its value. If the latter corresponds to a
physical magnitude, the units can also be specified after the value.
Names can be long and should be descriptive of the value it corresponds
to. FDF blocks are used to input structured data, in which case, the
program using FDF reads the inside of the block.

From the programming point of view, FDF allows for any data to be
retrieved whenever, from any part of the code, and in any order.

If a piece of data sought by FDF is not found in the input file, FDF
will return a default value, as set up in the call to the FDF routine.

Usage
-----

Here we explain the way it works in an example. Consider the following
input file:

``` {.text}
lattice_constant  5.5 Ang      # LatticeConstant == lattice_constant
NumberOfSpecies  2             # Order of labels is irrelevant
%block ChemicalSpeciesLabel
 1  8  O      # Species index, atomic number, species label
 2  1  H
%endblock ChemicalSpeciesLabel

spin.polarized                 # T==true==yes==blank
```

It illustrates typical input, including different features, such as
comments. Empty lines do not matter, neither do the number of spaces (or
positioning) between name and value.

Names are case-insensitive, and allow the introducing `-`, `_` or `=`.
in them for clarity without their being considered.

The position of any input pair is irrelevant.

A detailed description of the format can be found
[here]({{< ref "/data/fdf/_index.md" >}}).

Example
-------

Here is an example of source code that would read that input using the
FDF library:

``` {.fortran}
  use fdf
  type(block_fdf)            :: bfdf
  type(parsed_line), pointer :: pline

  ns       = fdf_get('NumberOfSpecies',0)   ! fdf_get(label,default[,units])
  latconst = fdf_get('LatticeConstant',0.0_dp,'Bohr')
  spinpol  = fdf_get('SpinPolarized',.false.)

  found = fdf_block('ChemicalSpeciesLabel',bfdf) ! read block into bfdf struct
  if (.not.found) stop "Block ChemicalSpeciesLabel not found"
  do is = 1,ns                         ! loop on species
    found = fdf_bline(bfdf,pline)      ! read one line into pline structure
    label(is) = fdf_bnames(pline,1)    ! first name in line
    z(is) = fdf_bintegers(pline,2)     ! second integer in line
  enddo
```
