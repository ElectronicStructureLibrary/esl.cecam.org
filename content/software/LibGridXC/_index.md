+++
title = "LibGridXC"
date =  2020-05-12T21:28:40+01:00
weight = 20
tags = ['ESL entries', 'ESL Software', 'DFT', 'DFT functionals']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| BSD | [Jose M. Soler](http://www.uam.es/departamentos/ciencias/fismateriac/pagesmem/jose_soler.html) | [Gitlab](https://gitlab.com/siesta-project/libraries/libgridxc/-/releases) |

LibGridXC is a library that receives the electron density values on the
points of an arbitrary regular grid, and gives back the values of the XC
potential and energy density on the same points in addition to the total
XC energy. It is ready to use by any code using regular grids (e.g.
plane-wave codes).

It encodes several local and semilocal density functionals (LDA and
GGAs), and several non-local ones as defined for Van der Waals,
including the original one by Dion et al (Phys. Rev. Lett. 2004) and its
later variants from the same and other groups. It also includes the Van
der Waals functional of Van Voorhis. All of them encoded using the
efficient factorisation presented in G. Román and J.M. Soler, Phys. Rev.
Lett. 2010 (the extension to the Van Voorhis functional is briefly
described in F. Corsetti et al, J. Chem. Phys. 2013).

The recent linking with [Libxc](/software/libxc/) expands the LDA and GGA
capabilities of LibGridXC to the large collection of local and semilocal
functionals encoded in [Libxc](/software/libxc/).

Current developments
--------------------

-   Link [libxc](/software/libxc/) under LibGridXC.
    The first one gives access to a large number of LDAs and GGAs in a
    point by point basis. The latter gives the xc energy and potential
    on a regular grid, being given the density on the same grid,
    including the various already coded non-local xc functional for Van
    der Waals. Micael Oliveira.

-   With the above linking, Siesta will immediately benefit from all the
    LDAs and GGAs coded in Libxc. Similarly, codes using regular grids
    can link to gridxc/libxc and get LDAs, GGAs and VDWs. ([José M.
    Soler](http://www.uam.es/departamentos/ciencias/fismateriac/pagesmem/jose_soler.html)).
