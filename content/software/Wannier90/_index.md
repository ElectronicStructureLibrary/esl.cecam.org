+++
title = "Wannier90"
date =  2020-05-12T21:28:40+01:00
weight = 65
tags = ['ESL entries', 'Other Software', 'Localized orbitals', 'Post-processing']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| GPL | [Wannier90 development team](http://www.wannier.org/people.html) | [Github](https://github.com/wannier-developers/wannier90/) |

Wannier90 is a code for computing the maximally-localized Wannier
functions (MLWFs) of
a system. It requires a separate electronic structure code to compute
and provide information on the Kohn-Sham energy bands. It can operate
either as a standalone post-processing utility, reading this information
from files, or as a library to be called internally by the electronic
structure code.

Wannier90 has a number of features, including a disentanglement scheme
for entangled energy bands, optimized Γ-point routines, plotting of the
MLWFs, Wannier interpolation to obtain many spectral and Fermi-surface
properties at high-resolution in the Brillouin zone, and others. A
complete list of features is maintained
[here](http://www.wannier.org/features/).

Algorithm
---------

Wannier90 computes MLWFs following the method of Marzari and
Vanderbilt[^1] (MV). For entangled energy bands, the method of Souza,
Marzari, and Vanderbilt[^2] (SMV) is used. The implementation of these
methods for Wannier90 is described here[^3], and in the [user
guide](http://www.wannier.org/support/) (Chapter 1).

Programming interface
---------------------

The interface for using Wannier90 as a library is described in the [user
guide](http://www.wannier.org/support/) (Chapter 6).

Installation
------------

### Prerequisites

-   Fortran 95 compiler
-   BLAS + LAPACK
-   MPI (optional for parallel execution of some post-processing
    utilities)

### Instructions

The system dependent parameters (e.g. compiler name) are contained in a
file called `make.sys`. Sample files may be found in the `./config/`
directory. Choose one that is nearest to your setup and copy it to
`make.sys`. You may need to edit it, for example to give the correct
library path for your machine.

On a Linux system with the Intel compiler, build Wannier90 by typing:

``` {.bash}
cp ./config/make.sys.ifort ./make.sys
make
```

For more detailed installation instructions, see the `README.install`
file in the source code.

### Tests

To run the test cases, type:

``` {.bash}
make test
```

References
----------

[^1]: N. Marzari and D. Vanderbilt, *Maximally localized generalized
    Wannier functions for composite energy bands*, Phys. Rev. B **56**,
    12847 (1997). DOI:
    [10.1103/PhysRevB.56.12847](http://dx.doi.org/10.1103/PhysRevB.56.12847)

[^2]: I. Souza, N. Marzari, and D. Vanderbilt, *Maximally localized
    Wannier functions for entangled energy bands*, Phys. Rev. B **65**,
    035109 (2001). DOI:
    [10.1103/PhysRevB.65.035109](http://dx.doi.org/10.1103/PhysRevB.65.035109)

[^3]: A. A. Mostofi, J. R. Yates, Y.-S. Lee, I. Souza, D. Vanderbilt,
    and N. Marzari, *`wannier90`: A Tool for Obtaining
    Maximally-Localised Wannier Functions*, Comput. Phys. Commun.
    **178**, 685 (2008). DOI:
    [10.1016/j.cpc.2007.11.016](http://dx.doi.org/10.1016/j.cpc.2007.11.016)
