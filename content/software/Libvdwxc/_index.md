+++
title = "Libvdwxc"
date =  2020-05-12T21:28:40+01:00
weight = 40
tags = ['ESL entries', 'ESL Software', 'Information']
+++

| License | Authors | Download | Documention |
|:--------|:--------|:---------|:------------|
| GPL v3 | A. H. Larsen, M. Kuisma, P. Erhart and Y. Pouillon | [Gitlab](https://gitlab.com/libvdwxc/libvdwxc/tree/master) | [website](http://libvdwxc.org) |

Libvdwxc is a general library for evaluating the energy and potential
for exchange-correlation (XC) functionals from the vdW-DF family. It can
be used with various of density functional theory (DFT) codes. It was
originally inspired by the success of
[LibXC](/software/libxc), a library of local and
semi-local XC functionals. At the moment, Libvdwxc provides access to
the DF1, DF2, and CX functionals, as well as interfaces for
[GPAW](https://wiki.fysik.dtu.dk/gpaw/) and
[Octopus](http://octopus-code.org/). The library has been tested with
respect to the S22 test set, various bulk properties of metals and
semiconductors, and surface energies.

Installation
------------

### Prerequisites

Libvdwxc makes an intensive use of Fast Fourier Transforms (FFT) and
requires:

-   FFTW (3.3.4 or upper).
-   PFFT on massively parallel architectures.

### Instructions

To build and install Libvdwxc, please refer to its documentation:
<https://libvdwxc.org>.

Publications
------------

The main paper describing this software can be found in this
reference[^1].

### References

[^1]: A.H. Larsen, M. Kuisma, J. Löfgren, Y. Pouillon, P. Erhart, P.
    Hyldgaard, *libvdwxc: A library for exchange--correlation
    functionals in the vdW-DF family*, arXiv:1703.06999v1 (2017)
