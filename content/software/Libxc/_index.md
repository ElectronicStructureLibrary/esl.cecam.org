+++
title = "Libxc"
date =  2020-05-12T21:28:40+01:00
weight = 45
tags = ['ESL Software']
+++

| License | Authors | Download | Documentation | Website |
|:--------|:--------|:---------|:--------------|:--------|
| [MPL2](https://www.mozilla.org/en-US/MPL/2.0/) | [Libxc Developers](https://www.tddft.org/programs/libxc/credits/) | [Download Page](https://www.tddft.org/programs/libxc/download/) | [Manual](https://www.tddft.org/programs/libxc/manual/) | https://www.tddft.org/programs/libxc |

Libxc is a library of exchange-correlation functionals for density-functional
theory. The aim is to provide a portable, well tested and reliable set of
exchange and correlation functionals that can be used by all the DFT codes.


Description
-----------

In Libxc you can find different types of functionals: LDA, GGA, hybrids, and
mGGA. These functionals depend on local information, in the sense that the value
of the potential at a given point depends only on the values of the density --
and the gradient of the density and the kinetic energy density, for the GGA and
mGGA cases -- at a given point:

$$E^{\rm LDA}\_{\rm xc} = E^{\rm LDA}\_{\rm xc}[n(\vec{r})]\\,,$$

$$E^{\rm GGA}\_{\rm xc} = E^{\rm GGA}\_{\rm xc}[n(\vec{r}), \vec{\nabla}n(\vec{r})]\,,$$

$$E^{\rm Hyb}\_{\rm xc} = a_x E^{\rm EXX} + E^{\rm GGA}\_{\rm xc}[n(\vec{r}), \vec{\nabla}n(\vec{r})]\\,,$$

$$E^{\rm mGGA}\_{\rm xc} = E^{\rm mGGA}\_{\rm xc}[n(\vec{r}), \vec{\nabla}n(\vec{r}), \nabla^2 n(\vec{r}), \tau(\vec{r})]\\,.$$

It can calculate the functional itself and its derivative. Higher-order
derivatives up to third-order are available for most functionals.


Publications
------------

The library is described in detail in the following papers:

* Susi Lehtola, Conrad Steigemann, Micael J.T. Oliveira, and Miguel A.L. Marques, *Recent developments in Libxc - A comprehensive library of functionals for density functional theory*, Software X **7**, 1 (2018). DOI: [10.1016/j.softx.2017.11.002](http://dx.doi.org/10.1016/j.softx.2017.11.002)

* Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, *Libxc: a library of exchange and correlation functionals for density functional theory*, Comput. Phys. Commun. **183**, 2272 (2012). DOI: [10.1016/j.cpc.2012.05.007](http://dx.doi.org/10.1016/j.cpc.2012.05.007)
