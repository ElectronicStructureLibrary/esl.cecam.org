+++
title = "PEXSI"
date =  2020-05-12T21:28:40+01:00
weight = 60
tags = ['ESL entries', 'Other Software', 'Sparse matrices', 'Localized orbitals']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| BSD modified | [PEXSI contributors](https://pexsi.readthedocs.io/en/latest/introduction.html#contributors) | [Project page](https://pexsi.readthedocs.io/en/latest/download.html) |

The **P**ole **EX**pansion and **S**elected **I**nversion (PEXSI) method is
a fast method for electronic structure calculation based on Kohn-Sham
density functional theory. It efficiently evaluates certain selected
elements of matrix functions, *e.g.*, the Fermi-Dirac function of the KS
Hamiltonian, which yields a density matrix. It can be used as an alternative
to diagonalization methods for obtaining the density, energy and forces in
electronic structure calculations. The PEXSI library is written in C++, and
uses message passing interface (MPI) to parallelize the computation on
distributed memory computing systems and achieve scalability on more than
10,000 processors.
