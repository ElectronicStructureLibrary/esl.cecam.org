+++
title = "ELPA"
date =  2020-05-12T21:28:40+01:00
weight = 55
tags = ['ESL entries', 'Other Software', 'Solvers']
+++

| License | Authors | Download |
|:--------|:--------|:---------|
| [LGPL](https://www.gnu.org/copyleft/lesser.html) | [ELPA Consortium](https://elpa.mpcdf.mpg.de/) | [Project page](https://elpa.mpcdf.mpg.de/software) |

**ELPA: Eigenvalue SoLver for Petaflop Applications**

The publicly available ELPA library provides highly efficient and highly scalable direct eigensolvers for symmetric matrices.
Though especially designed for use for PetaFlop/s applications solving large problem sizes on massively parallel supercomputers,  ELPA eigensolvers have proven to be also very efficient for smaller matrices.
All major architectures are supported.
